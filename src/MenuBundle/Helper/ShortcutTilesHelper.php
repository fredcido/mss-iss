<?php

namespace MenuBundle\Helper;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Description of ShortcutTilesHelper
 *
 * @author AdrianoRC
 */
class ShortcutTilesHelper implements ContainerAwareInterface {
    use ContainerAwareTrait;
    
    public function getMostAccessedItens($howMany = 10) {
        $em = $this->container->get('doctrine');
        $mainMenu = $this->container->get("app.menu.main_menu");
        
        $repository = $em->getRepository('MenuBundle:RouteAccessCount');
        
        // pega os itens mais acessados
        $topRoutes = $repository->getMostAccessedItens($howMany);
        
        // acha o item de menu correspondente pela URI
        $arrItens = [];
        foreach($topRoutes as $route) {
            $i = $mainMenu->findItemByUri($route->getRoute());
            if ($i) {
                $arrItens[] = $i;
            }
        }
        
        return $arrItens;
    }
    
}
