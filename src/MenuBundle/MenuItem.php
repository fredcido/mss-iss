<?php

namespace MenuBundle;

/**
 * This class represents one menu item
 *
 * @author adriano
 */
class MenuItem {

    /**
     * Label to output, name is used by default
     * @var string
     */
    protected $label = null;
    
    /**
     * URI
     * 
     * @var string
     */
    protected $uri = null;
    
    /**
     * Role access level
     * 
     * @var string
     */
    protected $role = null;
    
    /**
     * Array of children
     * 
     * @var array
     */
    protected $children = [];
    
    /**
     * How many children per column
     * 
     * @var integer
     */
    protected $numberOfChildrenPerColumn = null;
    
    /**
     *
     * @var string 
     */
    protected $listItemClass = null;
    
    /**
     *
     * @var string
     */
    protected $iconClass = null;
    
    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }
    
    public function getIconClass()
    {
        return $this->iconClass;
    }

    public function setIconClass($iconClass)
    {
        $this->iconClass = $iconClass;
        return $this;
    }
    
    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }
    
    public function getUri()
    {
        return $this->uri;
    }

    public function setUri($uri)
    {
        $this->uri = $uri;
        return $this;
    }
    
    /**
     * Add child
     * 
     * @param array $child
     */
    public function addChild($child) {
        $this->children[] = $child;
    }
    
    /**
     * Add children items
     * 
     * @param array $children
     */
    public function addChildren($children) {
        $this->children = $children;
    }
    
    /**
     * Returns the number of children 
     * 
     * @return integer
     */
    public function countChildren() {
        return count($this->children);
    }
    
    /**
     * Return true if current menu item has children
     * 
     * @return bool
     */
    public function hasChildren() {
        return $this->countChildren() > 0;
    }
    
    /**
     * Return the list of children
     * 
     * @return array
     */
    public function getChildren() {
        return $this->children;
    }
    
    public function setChildrenPerColumn($number) {
        $this->numberOfChildrenPerColumn = $number;
    }
    
    public function getChildrenPerColumn() {
        return $this->numberOfChildrenPerColumn;
    }
    
    /**
     * Returns children groupd by each $number
     * 
     * @param array $number
     */
    public function groupChildrenBy($number) {
        return array_chunk($this->children, $number);
    }
    
    public function setListItemClass($class) {
        $this->listItemClass = $class;
    }
    
    public function getListItemClass() {
        return $this->listItemClass;
    }

}
