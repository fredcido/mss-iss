<?php

namespace MenuBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;

/**
 * The purpose of this class is to count
 * how many times such route was accessed
 *
 * @author AdrianoRC
 */
class RouteAccessListener  implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $em = null;
    
    /**
     * @var LoggerInterface
     */
    private $logger;
    

    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }
        
        $request = $event->getRequest();
        $uri = $request->getRequestUri();
        
        if (!$this->isValidMenuUri($uri)) {
            return;
        }
        
        $route = $this->em->getRepository('MenuBundle:RouteAccessCount')->findOneBy(
            ['route' => $uri]
        );
        
        if (!$route) {
            $newRoute = new \MenuBundle\Entity\RouteAccessCount();
            $newRoute->setRoute($uri);
            $newRoute->setCount(1);
            $this->em->persist($newRoute);
            $this->em->flush();
        } else {
            $route->setCount($route->getCount() + 1);
            $this->em->persist($route);
            $this->em->flush();
        }
        
    }
    
    /**
     * Check if $uri is a valid menu uri.
     * 
     * @param string $uri
     */
    protected function isValidMenuUri($uri) {
        $mainMenu = $this->container->get("app.menu.main_menu");
        return $mainMenu->isValidMenuUri($uri);
    }
}