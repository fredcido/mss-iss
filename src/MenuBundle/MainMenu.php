<?php

namespace MenuBundle;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * This class consctructs the main navbar menu
 *
 * @author AdrianoRC
 */
class MainMenu implements ContainerAwareInterface {
    use ContainerAwareTrait;
    
    /**
     * MenuItens
     * 
     * @var array
     */
    private $_mainMenuItens = [];
    
    /**
     * Symfony translator service
     */
    private $translator = null;
    
    /**
     * Symfony router
     * 
     */
    private $router = null;
    
    
    /**
     * Constructor
     */
    public function __construct($container) {
        $this->setContainer($container);
        $this->translator = $container->get('translator');
        $this->router = $container->get('router');
        
        $this->buildMenu();
    }
    
    /**
     * Builds the menu hierarchy
     */
    protected function buildMenu() {
        // shortcuts
        $router = $this->router;
        $trans = $this->translator;
        
        // home menu
        $home = new MenuItem();
        $home->setLabel($trans->trans('home', [], 'app'))
             ->setUri($router->generate('homepage'))
             ->setIconClass('fa fa-home');
        $this->addItem($home);
        
        // settings menu
        $settings = new MenuItem();
        $settings->setLabel($trans->trans('settings', [], 'app'));
        $settings->setRole('ROLE_ADMIN');
        $settings->setChildrenPerColumn(5);
        $settings->setIconClass('fa fa-cog');
        $settings->setListItemClass("mega-menu-dropdown");
        
        $settings->addChildren([
            (new MenuItem())
                ->setLabel($trans->trans('users', [], 'app'))
                ->setUri($router->generate('users_index'))
                ->setIconClass('fa fa-users'),
            
            (new MenuItem())
                ->setLabel($trans->trans('Program', [], 'app'))
                ->setUri($router->generate('program_index'))
                ->setIconClass('fa fa-folder-open-o'),
            
            (new MenuItem())
                ->setLabel($trans->trans('InterventionAreas', [], 'app'))
                ->setUri($router->generate('intervention_area_index')),
            
            (new MenuItem())
                ->setLabel($trans->trans('SocialResponse', [], 'app'))
                ->setUri($router->generate('social_response_index')),
            
            (new MenuItem())
                ->setLabel($trans->trans('BeneficiaryClass', [], 'app'))
                ->setUri($router->generate('beneficiary_class_index'))
                ->setIconClass('fa fa-user'),

            (new MenuItem())
                ->setLabel($trans->trans('ExpenseReference', [], 'app'))
                ->setUri($router->generate('expense_reference_index')),
            
            /*(new MenuItem())
                ->setLabel($trans->trans('ExpenseType', [], 'app'))
                ->setUri($router->generate('expense_type_index')),*/

            (new MenuItem())
                ->setLabel($trans->trans('Expense', [], 'app'))
                ->setUri($router->generate('expense_index')),

            (new MenuItem())
                ->setLabel($trans->trans('RelationshipExpense', [], 'app'))
                ->setUri($router->generate('relationship_expense_index')),
            
            (new MenuItem())
                ->setLabel($trans->trans('WorkflowType', [], 'app'))
                ->setUri($router->generate('workflow_type_index')),

            (new MenuItem())
                ->setLabel($trans->trans('Post', [], 'app'))
                ->setUri($router->generate('post_index')),

            (new MenuItem())
                ->setLabel($trans->trans('Bank', [], 'app'))
                ->setUri($router->generate('bank_index')),

            (new MenuItem())
                ->setLabel($trans->trans('GeneralRevenueSource', [], 'app'))
                ->setUri($router->generate('general_revenue_source_index')),
            
            (new MenuItem())
                ->setLabel($trans->trans('RevenueSource', [], 'app'))
                ->setUri($router->generate('revenue_source_index')),

            (new MenuItem())
                ->setLabel($trans->trans('settings', [], 'app'))
                ->setUri($router->generate('setting_index')),
        ]);
        $this->addItem($settings);
        
        // iss menu
        $iss = new MenuItem();
        $iss->setLabel($trans->trans('SocialInstitution', [], 'app'))
             ->setUri($router->generate('social_institution_index'))
             ->setRole('ROLE_USER')
             ->setIconClass('fa fa-building');
        $this->addItem($iss);
        
        // menu de exemplo
        
        /*$sample = new MenuItem();
        $sample->setLabel($trans->trans('Exemplo', [], 'app'));
        $sample->setRole('ROLE_USER');
        $sample->setListItemClass("");
        
        $sample->addChild(
            (new MenuItem())
                ->setLabel('Item 1')
                ->setUri('http://www.google.com.br')
        );
        $sample->addChild(
            (new MenuItem())
                ->setLabel('Item 2')
                ->setUri('http://www.google.com.br')
        );
        
        $sub = new MenuItem();
        $sub->setLabel('Submenu');
        $sub->setIconClass("icon-settings");
        $sub->addChild(
            (new MenuItem())
                ->setLabel('Subitem 1')
                ->setUri('http://www.google.com.br')
        );
        $sub->addChild(
            (new MenuItem())
                ->setLabel('Subitem 2')
                ->setUri('http://www.google.com.br')
        );
        $sample->addChild($sub);
        
        $this->addItem($sample);*/
    }
    
    /**
     * Add item to the list
     * 
     * @param \MenuBundle\MenuItem $item
     */
    protected function addItem(MenuItem $item) {
        $this->_mainMenuItens[] = $item;
    }
    
    /**
     * 
     */
    public function getMainMenu() {
        return $this->_mainMenuItens;
    }
    
    /**
     * Return all chindren uri
     * @param \MenuBundle\MenuItem $item
     */
    public function getChildrenUri(MenuItem $item) {
        
        $uris = [];
        
        if ($item->getUri()) {
            $uris[] = $item->getUri();
        }
        
        if (!$item->hasChildren()) {
            return $uris;
        }
        
        foreach($item->getChildren() as $child) {
            $uris = array_merge($uris, $this->getChildrenUri($child));
        }
        
        return $uris;
    }
    
    /**
     * Check if $uri is a valid menu uri
     * 
     * @param string $uri
     */
    public function isValidMenuUri($uri) {
        
        $avaliableUri = [];
        
        foreach($this->_mainMenuItens as $item) {
            if ($item->getUri()) {
                $avaliableUri[] = $item->getUri();
            }
            
            if ($item->hasChildren()) {
                $avaliableUri = array_merge($avaliableUri, $this->getChildrenUri($item));
            }
        }
        
        return in_array($uri, $avaliableUri);
    }
    
    
    protected function findItemInCollectionByUri($collection, $uri) {
        foreach($collection as $menuItem) {
            if ($menuItem->getUri() == $uri) {
                return $menuItem;
            }
            
            if ($menuItem->hasChildren()) {
                $foundItem = $this->findItemInCollectionByUri($menuItem->getChildren(), $uri);
                
                if ($foundItem != null) {
                    return $foundItem;
                }
            }
            
        }
        
        return null;
    }
    
    /**
     * 
     * @param MenuItem $uri
     */
    public function findItemByUri($uri) {
        return $this->findItemInCollectionByUri($this->getMainMenu(), $uri);
    }
}
