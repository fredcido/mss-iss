<?php

namespace MenuBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;


/**
 * Description of MenuExtension
 *
 * @author AdrianoRC
 */
class MenuExtension extends \Twig_Extension implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    
    public function getFunctions()
    {
        return array(
             new \Twig_SimpleFunction('app_menu_render', array($this, 'render'), array('is_safe' => array('html'))),
             new \Twig_SimpleFunction('app_menu_render_item', array($this, 'renderItem'), array('is_safe' => array('html'))),
             new \Twig_SimpleFunction('app_menu_render_single', array($this, 'renderSingle'), array('is_safe' => array('html'))),
             new \Twig_SimpleFunction('app_menu_render_children', array($this, 'renderChildren'), array('is_safe' => array('html'))),
             new \Twig_SimpleFunction('app_menu_render_shortcut_tiles', array($this, 'renderShortcutTiles'), array('is_safe' => array('html'))),
             new \Twig_SimpleFunction('app_menu_render_icon', array($this, 'renderIcon'), array('is_safe' => array('html'))),
        );
    }
    
    public function render() {
        $mainMenu = $this->container->get("app.menu.main_menu");
        
        return $this->container->get('twig')->render(
                'MenuBundle::main.html.twig',
                ['mainMenu' => $mainMenu->getMainMenu()]
        );
    }
    
    public function renderItem($item) {
        return $this->container->get('twig')->render(
                'MenuBundle::menu-item.html.twig',
                ['item' => $item]
        );
    }
    
    public function renderSingle($item) {
        return $this->container->get('twig')->render(
                'MenuBundle::menu-single.html.twig',
                ['item' => $item]
        );
    }
    
    public function renderChildren($item) {
        return $this->container->get('twig')->render(
                'MenuBundle::menu-children.html.twig',
                ['item' => $item]
        );
    }
    
    public function renderShortcutTiles() {
        $helper = $this->container->get("app.menu.shortcut_tiles_helper");
        
        return $this->container->get('twig')->render(
                'MenuBundle::shortcut-tiles.html.twig',
                ['itens' => $helper->getMostAccessedItens(4)]
        );
    }
    
    public function renderIcon() {
        $mainMenu = $this->container->get("app.menu.main_menu");
        
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getCurrentRequest();
        $currentUri = $request->getRequestUri();

        $menuItem = null;
        
        // $currentUri is something like '/program/'
        if (preg_match("#^/[a-z0-9_\-]+/?$#i", $currentUri)) {
            $menuItem = $mainMenu->findItemByUri($currentUri);
        }
        
        // $currentUri is something like '/program/new/'
        if (preg_match("#^(/[a-z0-9_\-]+/)([a-z0-9_\-]+/?)$#i", $currentUri, $matches)) {
            $menuItem = $mainMenu->findItemByUri($matches[1]);
        }
        
        // $currentUri is something like '/program/1/edit/'
        if (preg_match("#^(/[a-z0-9_\-]+/)([0-9]+/)([a-z0-9_\-]+/?)$#i", $currentUri, $matches)) {
            $menuItem = $mainMenu->findItemByUri($matches[1]);
        }

        if ($menuItem !== null && $menuItem->getIconClass()) {
            return $menuItem->getIconClass();
        }
        
        return '';
    }
    
    public function getName()
    {
        return 'app_extension';
    }
}
