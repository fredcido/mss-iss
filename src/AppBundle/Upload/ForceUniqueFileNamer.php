<?php

namespace AppBundle\Upload;

use Cocur\Slugify\Slugify;

class ForceUniqueFileNamer implements FileNamer
{
    /**
     * @var string
     */
    protected $folder;

    /**
     * Gets the value of folder.
     *
     * @return string
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Sets the value of folder.
     *
     * @param string $folder the folder
     *
     * @return self
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
        return $this;
    }

    /**
     * @param  Uploadable $entity
     * @return string
     */
    public function getFileName(Uploadable $entity, \SplFileInfo $file)
    {
        $slugger = new Slugify();
        $extension = strtolower($file->getExtension());
        $name = $file->getBasename('.' . $extension);

        $suffix = 0;
        do {
            $fileName = $slugger->slugify($name);
            $formedName = $fileName . ($suffix > 0 ? '_' . $suffix : '') . '.' . $extension;
            $fullpath = $this->folder . '/' . $formedName ;
            $suffix++;
        } while (file_exists($fullpath));

        return $formedName;
    }
}
