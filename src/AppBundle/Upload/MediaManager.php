<?php

namespace AppBundle\Upload;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;

class MediaManager
{
    use ContainerAwareTrait;

    /**
     * @var FileNamer
     */
    protected $fileNamer;

    /**
     * @var FolderNamer
     */
    protected $folderNamer;

    /**
     * @var string
     */
    protected $uploadFolder;

    /**
     * Gets the value of fileNamer.
     *
     * @return FileNamer
     */
    public function getFileNamer()
    {
        return $this->fileNamer;
    }

    /**
     * Sets the value of fileNamer.
     *
     * @param FileNamer $fileNamer the file namer
     *
     * @return self
     */
    public function setFileNamer(FileNamer $fileNamer)
    {
        $this->fileNamer = $fileNamer;

        return $this;
    }

    /**
     * Gets the value of folderNamer.
     *
     * @return FolderNamer
     */
    public function getFolderNamer()
    {
        return $this->folderNamer;
    }

    /**
     * Sets the value of folderNamer.
     *
     * @param FolderNamer $folderNamer the folder namer
     *
     * @return self
     */
    public function setFolderNamer(FolderNamer $folderNamer)
    {
        $this->folderNamer = $folderNamer;

        return $this;
    }

    /**
     * Gets the value of uploadFolder.
     *
     * @return string
     */
    public function getUploadFolder()
    {
        return $this->uploadFolder;
    }

    /**
     * Sets the value of uploadFolder.
     *
     * @param string $uploadFolder the upload folder
     *
     * @return self
     */
    public function setUploadFolder($uploadFolder)
    {
        $this->uploadFolder = $uploadFolder;
        return $this;
    }

    /**
     * @param  Uploadable $entity
     * @return string
     */
    protected function getFullDirPath(Uploadable $entity)
    {
        $folderName = $this->folderNamer->getFolderName($entity);
        $fullDirPath = realpath(rtrim($this->uploadFolder, '/') . '/' . trim($folderName, '/')) . '/';

        if (!is_dir($fullDirPath)) {
            mkdir($fullDirPath, 0755, true);
        }

        return $fullDirPath;
    }

    /**
     * @param  Uploadable $entity
     * @param  Request    $request
     */
    public function upload(Uploadable $entity, Request $request)
    {
        $destinationDirectory = $this->getFullDirPath($entity);
        $files = $request->files->all();

        $this->fileNamer->setFolder($destinationDirectory);

        foreach ($files as $file) {
            $destinationFilename = $this->fileNamer->getFileName($file);
            $file->move($destinationDirectory, $destinationFilename);
        }
    }

    /**
     * @param  Uploadable $entity
     * @return array
     */
    public function getMedia(Uploadable $entity)
    {
        $path = $this->getFullDirPath($entity);

        $finder = new Finder();
        $finder->files()->in($path);

        $files = array();
        foreach ($finder as $file) {
            $files[] = $file;
        }

        return $files;
    }
}
