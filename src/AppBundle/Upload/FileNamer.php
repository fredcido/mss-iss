<?php

namespace AppBundle\Upload;

interface FileNamer
{
    public function setFolder($folder);

    public function getFileName(Uploadable $entity, \SplFileInfo $file);
}
