<?php

namespace AppBundle\Upload;

use Cocur\Slugify\Slugify;

class ClassFolderNamer implements FolderNamer
{
    /**
     * @param  Uploadable $entity
     * @return string
     */
    public function getFolderName(Uploadable $entity)
    {
        $className = get_class($entity);

        $slugger = new Slugify();
        $parentFolder = $slugger->slugify($className);

        return sprintf("%s/%s", $parentFolder, $entity->getId());
    }
}
