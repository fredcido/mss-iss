<?php

namespace AppBundle\Upload;

interface Uploadable
{
    public function getId();
}