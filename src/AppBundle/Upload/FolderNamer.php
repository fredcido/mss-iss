<?php

namespace AppBundle\Upload;

interface FolderNamer
{
    public function getFolderName(Uploadable $entity);
}
