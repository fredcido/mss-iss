<?php

namespace AppBundle\Service;

use AppBundle\Entity;
use AppBundle\Enum;
use Doctrine\ORM\Query\ResultSetMapping;

class RelationshipExpense extends ServiceAbstract
{
    /**
     * Get main ExpenseReference entity
     * 
     * @return Entity\ExpenseReference
     */
    public function getExpenseReferenceSource()
    {
        $repository = $this->getRepository('AppBundle:ExpenseReference');
        
        return $repository->findOneByFlag(Enum\Status::ACTIVE);
    }
    
    /**
     * 
     * @return array
     */
    public function getExpenseReferenceTargetCollection()
    {
        $repository = $this->getRepository('AppBundle:ExpenseReference');
        
        return $repository->findByFlag(Enum\Status::INACTIVE);
    }
    
    /**
     * 
     * @param Entity\ExpenseReference $entity
     * @return array
     */
    public function getExpenseByExpenseReference(Entity\ExpenseReference $entity)
    {
        $repository = $this->getRepository('AppBundle:Expense');
        
        return $repository->findByExpenseReference($entity);
    }

    /**
     * 
     * @param integer $id
     * @return array
     */
    public function getExpenseByIdExpenseReferenceTarget($id)
    {
        if (! is_numeric($id)) {
            return [];
        }
        
        if (null === ($source = $this->getExpenseReferenceSource())) {
            return [];
        }
        
        $qb = $this->getRepository('AppBundle:Expense')->createQueryBuilder('e');
        
        $qb->join('e.expenseTarget', 't')
           ->where($qb->expr()->eq('e.expenseReference', $source->getId()))
           ->andWhere($qb->expr()->eq('t.expenseReference', $id));
        
        return $qb->getQuery()->getResult();
    }
    
    /**
     * 
     * @return array
     */
    public function getExpenseSourceCollection()
    {
        if (null === ($source = $this->getExpenseReferenceSource())) {
            return [];
        }
        
        return $this->getExpenseByExpenseReference($source);
    }
    
    /**
     * 
     * @return array
     */
    public function getExpenseArrayByCollection(array $collection)
    {
        $expenses = array();
        
        /* @var $entity \AppBundle\Entity\Expense */
        foreach ($collection as $entity) {
            $expenses[] = [
                'value' => $entity->getId(),
                'text'  => (string) $entity,
            ];
        }
        
        return $expenses;
    }
    
    /**
     * 
     * @param integer $id
     * @throws \InvalidArgumentException
     * @return array
     */
    public function getExpenseTargetCollection($id)
    {
        if (! is_numeric($id)) {
            return [];
        }
        
        if (null === ($target = $this->getEntityManager()->find('AppBundle:ExpenseReference', $id))) {
            return [];
        }
        
        return $this->getExpenseByExpenseReference($target);
    }
    
    /**
     * 
     * @param array $data
     */
    protected function delete(array $data)
    {
        if (null === ($source = $this->getExpenseReferenceSource())) {
            return;
        }
        
        $em = $this->getEntityManager();

        $sql = 'DELETE re
                  FROM relationship_expense re
            INNER JOIN expense s ON s.id = re.expense_source_id
            INNER JOIN expense t ON t.id = re.expense_target_id
                 WHERE s.expense_reference_id = :source
                   AND t.expense_reference_id = :target';

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute([
            'source' => $source->getId(),
            'target' => $data['expense_reference']
        ]);
    }
    
    /**
     * 
     * @param array $data
     */
    public function save(array $data)
    {
        $data['expense_source'] = array_unique($data['expense_source']);
        
        $this->delete($data);

        $em = $this->getEntityManager();
        
        foreach ($data['expense_source'] as $sourceId) {
            if (empty($sourceId)) {
                continue;
            }
            
            /* @var $source \AppBundle\Entity\Expense */
            $source = $em->find('AppBundle:Expense', $sourceId);
            
            if (empty($data['expense_target'])) {
                break;
            }
            
            foreach ($data['expense_target'][$sourceId] as $targetId) {
                /* @var $target \AppBundle\Entity\Expense */
                $target = $em->find('AppBundle:Expense', $targetId);
                
                $source->addExpenseTarget($target);
                $em->persist($source);
            }
            
            $em->flush();
        }
    }
}