<?php

namespace AppBundle\Service;

use AppBundle\Message\Messages;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MessageBroker
{
    use ContainerAwareTrait;
    
    const TAG = 'messages';

    /**
     *
     * @param string $message
     * @param string $level
     */
    public function add($message, $level = 'danger')
    {
        $message = $this->container->get('app.messages')->get($message);

        $messageContainer = array(
            'msg'   => $message,
            'level' => $level,
        );

        $this->container->get('session')->getFlashBag()->add(self::TAG, $messageContainer);

        return $this;
    }

    /**
     *
     * @param string $message
     */
    public function addError($message)
    {
        $this->add($message, 'danger');
        return $this;
    }

    /**
     *
     * @param string $message
     */
    public function addSuccess($message)
    {
        $this->add($message, 'success');
        return $this;
    }

    /**
     *
     * @param string $message
     */
    public function addInfo($message)
    {
        $this->add($message, 'info');
        return $this;
    }

    /**
     *
     */
    public function clean()
    {
        $this->container->get('session')->getFlashBag()->clear();
        return $this;
    }
}
