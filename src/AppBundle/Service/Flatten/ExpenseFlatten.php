<?php

namespace AppBundle\Service\Flatten;

use AppBundle\Entity;

class ExpenseFlatten
{
    /**
     * 
     * @param Entity\Expense $entity
     * @return array
     */
    private function getData(Entity\Expense $entity)
    {
        return [
            'id'   => $entity->getId(),
            'text' => $this->flatten($entity),
        ];
    }
    
    /**
     *
     * @param EntityAbstract $entity
     * @param array $data
     * @param string $separator
     * @return string
     */
    private function flatten(Entity\Expense $entity, array $data = [], $separator = ' / ')
    {
        if (empty($data)) {
            $data[] = (string) $entity;
        }
    
        if (null !== ($parent = $entity->getParent())) {
            $data[] = (string) $parent;
            return $this->flatten($parent, $data, $separator);
        }
    
        return implode($separator, array_reverse($data));
    }
    
    /**
     * 
     * @param array $collection
     * @return array
     */
    public function toArray(array $collection)
    {
        $data = array();
        
        /* @var $entity Entity\Expense */
        foreach ($collection as $entity) {
            $data[] = $this->getData($entity);
        }
        
        return $data;
    }
}