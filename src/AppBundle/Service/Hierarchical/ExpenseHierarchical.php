<?php

namespace AppBundle\Service\Hierarchical;

use AppBundle\Entity;

/**
 *
 */
class ExpenseHierarchical
{
    /**
     * 
     * @param Entity\Expense $entity
     * @param boolean $disabled
     * @param integer $level
     * @return array
     */
    private function getData(Entity\Expense $entity, $disabled, $level = 0)
    {
        return [
            'id'       => $entity->getId(),
            'text'     => str_repeat('&nbsp;', ($level * 2)) . (string) $entity,
            'disabled' => $disabled,
        ];
    }
    
    /**
     * 
     * @param Entity\Expense $entity
     * @param array $data
     * @param integer $level
     */
    private function hierarchical(Entity\Expense $entity, array $data = [], $level = 1)
    {
        if (empty($data)) {
            $data[] = $this->getData($entity, true);
        }
        
        /* @var $child \AppBundle\Entity\Expense */
        $children = $entity->getChildren();
        
        foreach ($children as $child) {
            $isEmpty = $child->getChildren()->isEmpty();
            
            $data[] = $this->getData($child, (! $isEmpty), $level);
            
            if (!$isEmpty) {
                return $this->hierarchical($child, $data, ($level + 1));
            }
        }
        
        return $data;
    }
    
    public function toArray(Entity\Expense $entity)
    {
        return $this->hierarchical($entity);
    }
}