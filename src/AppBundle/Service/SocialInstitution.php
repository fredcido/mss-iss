<?php

namespace AppBundle\Service;

use AppBundle\Entity;

class SocialInstitution extends ServiceAbstract
{
    public function getData()
    {
        $repository = $this->getRepository(Entity\SocialInstitution::class);
        $collection = $repository->findAll();

        return $collection;
    }

    public function setFromArray(\AppBundle\Entity\SocialInstitution $entity, $data = array())
    {
        foreach ($data as $property => $value) {
            $method = "set{$property}";

            switch ($property) {
                case 'SocialInstitutionContact':

                    foreach($value as $v){
                        $socialInstitutionContact = new \AppBundle\Entity\SocialInstitutionContact();
                        $socialInstitutionContact->setName($v['name']);
                        $socialInstitutionContact->setEmail($v['email']);
                        $entity->addSocialInstitutionContact($socialInstitutionContact);
                    }
                    break;

                default:
                    $entity->$method($value);
            }
        }

        return $entity;
    }
}
