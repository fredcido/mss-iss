<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Messages
 *
 * Esta classe serve como um repositorio para as mensagens
 * que não estão esplícitas no código, por ex: javascript.
 *
 */
class Messages
{
    use ContainerAwareTrait;

    const LOADING                  = 'loading';
    const CHECK_REQUIRED_FIELDS    = 'check_required_fields';
    const MESSAGE_SUCCESS          = 'messages.success';
    const MESSAGE_ERROR            = 'messages.error';
    const MESSAGE_VALIDATION_ERROR = 'messages.validation_error';

    private $messages = [];

    /*
     * Constructor
     *
     */
    public function __construct(ContainerInterface $container = null)
    {
        $this->setContainer($container);

        $translator = $this->container->get('translator');

        $this->messages = [
            self::LOADING                  => $translator->trans('loading', [], 'app'),
            self::CHECK_REQUIRED_FIELDS    => $translator->trans('check_required_fields', [], 'app'),
            self::MESSAGE_SUCCESS          => $translator->trans('messages.success', [], 'app'),
            self::MESSAGE_ERROR            => $translator->trans('messages.error', [], 'app'),
            self::MESSAGE_VALIDATION_ERROR => $translator->trans('messages.validation_error', [], 'app'),
        ];
    }

    /*
     * Obtém mensagem pelo seu id
     *
     */
    public function get($messageKey)
    {
        if (isset($this->messages[$messageKey])) {
            return $this->messages[$messageKey];
        }

        return $messageKey;
    }

}
