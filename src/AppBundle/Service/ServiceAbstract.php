<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use pmill\Doctrine\Hydrator\ArrayHydrator;

/**
 * 
 */
abstract class ServiceAbstract
{
    /**
     * @var EntityManager
     */
    private $em;
    
    /**
     * 
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    /**
     * 
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->em;
    }
    
    /**
     * 
     * @param string $entityName
     * @return EntityRepository
     */
    protected function getRepository($entityName)
    {
        return $this->getEntityManager()->getRepository($entityName);
    }
    
    /**
     * 
     * @param string $entityName
     * @param array $data
     */
    public function hydrate($entityName, array $data)
    {
        $hydrator = new ArrayHydrator($this->getEntityManager());
        
        return $hydrator->hydrate($entityName, $data);
    }
}