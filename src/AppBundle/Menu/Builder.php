<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        // REMOVE OR USE
        
        // $menu->setChildrenAttribute('class', 'nav navbar-nav');
        //
        // $menu->addChild('Home',
        //     array(
        //         'route' => 'homepage', 'attributes' => array('class' => 'nav-link')
        //     )
        // );
        //
        // $menu->addChild('Usuários',
        //     array(
        //         'route' => 'users_index', 'attributes' => array('class' => 'nav-link')
        //     )
        // );
        //
        // $menu->addChild('Área de Intervenção',
        //     array(
        //         'route' => 'interventionarea_index', 'attributes' => array('class' => 'nav-link')
        //     )
        // );

        return $menu;
    }
}
