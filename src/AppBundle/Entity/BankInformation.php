<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * BankInformation
 *
 * @ORM\Table(name="bank_information", indexes={@ORM\Index(name="fk_bank_information_social_institution1_idx", columns={"social_institution_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BankInformation")
 */
class BankInformation extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Bank", inversedBy="bankInformation")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="bank_id", referencedColumnName="id", nullable=false)
     */
    private $bank;

    /**
     * @var string
     *
     * @ORM\Column(name="nib", type="string", length=21, nullable=true)
     * @Assert\Length(
     *      max = 21,
     *      maxMessage = "max_chars"
     * )
     */
    private $nib;

    /**
     * @var string
     *
     * @ORM\Column(name="iban", type="string", length=45, nullable=true)
     * @Assert\Length(
     *      max = 45,
     *      maxMessage = "max_chars"
     * )
     */
    private $iban;

    /**
     * @var \AppBundle\Entity\SocialInstitution
     *
     * @ORM\ManyToOne(targetEntity="SocialInstitution", inversedBy="bankInformation")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="social_institution_id", referencedColumnName="id", nullable=false)})
     * @JMS\Exclude()
     */
    private $socialInstitution;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BankInformation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nib
     *
     * @param string $nib
     *
     * @return BankInformation
     */
    public function setNib($nib)
    {
        $this->nib = $nib;

        return $this;
    }

    /**
     * Get nib
     *
     * @return string
     */
    public function getNib()
    {
        return $this->nib;
    }

    /**
     * Set iban
     *
     * @param string $iban
     *
     * @return BankInformation
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set socialInstitution
     *
     * @param \AppBundle\Entity\SocialInstitution $socialInstitution
     *
     * @return BankInformation
     */
    public function setSocialInstitution(SocialInstitution $socialInstitution = null)
    {
        $this->socialInstitution = $socialInstitution;

        return $this;
    }

    /**
     * Gets the value of bank.
     *
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Sets the value of bank.
     *
     * @param mixed $bank the bank
     *
     * @return self
     */
    public function setBank(Bank $bank)
    {
        $this->bank = $bank;

        return $this;
    }
}
