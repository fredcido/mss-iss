<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PostoAdministrativo
 *
 * @ORM\Table(name="posto_administrativo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostoAdministrativoRepository")
 */
class PostoAdministrativo extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Municipio", inversedBy="postosAdministrativos")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="municipio_id", referencedColumnName="id", nullable=false)
     */
    private $municipio;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Suku", mappedBy="postoAdministrativo")
     */
    private $sukus;

    public function __construct()
    {
        $this->sukus = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PostoAdministrativo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add sukus
     *
     * @param Suku $sukus
     * @return Race
     */
    public function addSuku(Suku $suku)
    {
        $this->sukus[] = $suku;
        return $this;
    }

    /**
     * Remove suku
     *
     * @param Suku $suku
     */
    public function removeSuku(Suku $suku)
    {
        $this->sukus->removeElement($suku);
    }

    /**
     * Get sukus
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSukus()
    {
        return $this->sukus;
    }

    public function getMunicipio()
    {
        return $this->municipio;
    }
}
