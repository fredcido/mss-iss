<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RevenueSource
 *
 * @ORM\Table(name="revenue_source")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RevenueSourceRepository")
 *
 * @UniqueEntity(fields="name", message="This value is already in use, choose other.")
 */
class RevenueSource extends EntityAbstract
{
    use EntityAudit;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GeneralRevenueSource", inversedBy="revenueSources")
     * @ORM\JoinColumn(name="general_revenue_source_id", referencedColumnName="id", onDelete="set null")
     */
    private $generalRevenueSource;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, unique=true)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "max_chars"
     * )     
     */
    private $name;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 1;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RevenueSource
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return \AppBundle\Entity\RevenueSource
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }
    
    /**
     * Get status
     *
     * @return \AppBundle\Entity\smallint
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set generalRevenueSource
     *
     * @param \AppBundle\Entity\GeneralRevenueSource $generalRevenueSource
     *
     * @return RevenueSource
     */
    public function setGeneralRevenueSource(\AppBundle\Entity\GeneralRevenueSource $generalRevenueSource = null)
    {
        $this->generalRevenueSource = $generalRevenueSource;

        return $this;
    }

    /**
     * Get generalRevenueSource
     *
     * @return \AppBundle\Entity\GeneralRevenueSource
     */
    public function getGeneralRevenueSource()
    {
        return $this->generalRevenueSource;
    }
}
