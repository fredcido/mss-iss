<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * WorkflowType
 *
 * @ORM\Table(name="workflow_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WorkflowTypeRepository")
 * 
 * @UniqueEntity(fields="name", message="This value is already in use, choose other.")
 */
class WorkflowType extends EntityAbstract
{
    use EntityAudit;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, unique=true)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "max_chars"
     * )     
     */
    private $name;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 1;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return WorkflowType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     * 
     * @param integer $status
     * @return \AppBundle\Entity\Program
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }
    
    /**
     * Get status
     * 
     * @return \AppBundle\Entity\smallint
     */
    public function getStatus()
    {
        return $this->status;
    }
}
