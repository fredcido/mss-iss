<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * InstitutionStatus
 *
 * @ORM\Table(name="institution_status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InstitutionStatusRepository")
 */
class InstitutionStatus extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * 
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     */
    private $name;

//    /**
//     * @ORM\OneToMany(targetEntity="SocialInstitution", mappedBy="institutionStatus")
//     */
//    private $socialInstitutions;

    /**
     * Constructor
     */
    public function __construct()
    {
//        $this->socialInstitutions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InstitutionStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add bankInformation
     *
     * @param BankInformation $bankInformation
     * @return Race
     */
//    public function addSocialInstitution(SocialInstitution $socialInstitutions)
//    {
//        $this->socialInstitutions[] = $socialInstitutions;
//        return $this;
//    }

    /**
     * Remove bankInformation
     *
     * @param BankInformation $bankInformation
     */
//    public function removeSocialInstitution(BankInformation $socialInstitutions)
//    {
//        $this->socialInstitutions->removeElement($socialInstitutions);
//    }

    /**
     * Get bankInformation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
//    public function getSocialInstitution()
//    {
//        return $this->socialInstitutions;
//    }
}
