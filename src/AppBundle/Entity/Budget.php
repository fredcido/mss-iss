<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Budget
 *
 * @ORM\Table(name="budget")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BudgetRepository")
 */
class Budget extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="date")
     */
    private $data;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="year", type="date")
     */
    private $year;

    /**
     * @var PostoAdministrativo
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PostoAdministrativo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="posto_administrativo_id", referencedColumnName="id")
     * })
     */
    private $postoAdministrativo;

    /**
     * @var Program
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Program")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="program_id", referencedColumnName="id")
     * })
     */
    private $program;

    /**
     * @var SocialInstitution
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SocialInstitution")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="social_institution_id", referencedColumnName="id")
     * })
     */
    private $socialInstitution;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Budget
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set year
     *
     * @param \DateTime $year
     *
     * @return Budget
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return \DateTime
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set postoAdministrativo
     *
     * @param PostoAdministrativo $postoAdministrativo
     *
     * @return Budget
     */
    public function setPostoAdministrativo(PostoAdministrativo $postoAdministrativo = null)
    {
        $this->postoAdministrativo = $postoAdministrativo;

        return $this;
    }

    /**
     * Get postoAdministrativo
     *
     * @return PostoAdministrativo
     */
    public function getPostoAdministrativo()
    {
        return $this->postoAdministrativo;
    }

    /**
     * Set program
     *
     * @param Program $program
     *
     * @return Budget
     */
    public function setProgram(Program $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return Program
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set socialInstitution
     *
     * @param SocialInstitution $socialInstitution
     *
     * @return Budget
     */
    public function setSocialInstitution(SocialInstitution $socialInstitution = null)
    {
        $this->socialInstitution = $socialInstitution;

        return $this;
    }

    /**
     * Get socialInstitution
     *
     * @return SocialInstitution
     */
    public function getSocialInstitution()
    {
        return $this->socialInstitution;
    }
}

