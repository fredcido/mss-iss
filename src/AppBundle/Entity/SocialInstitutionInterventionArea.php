<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SocialInstitution
 *
 * @ORM\Table(name="social_institution_intervention_area")})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SocialInstitutionInterventionAreaRepository")
 */
class SocialInstitutionInterventionArea extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SocialInstitution", inversedBy="interventionAreas")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="social_institution_id", referencedColumnName="id", nullable=false)
     */
    private $socialInstitution;

    /**
     * @ORM\ManyToOne(targetEntity="InterventionArea", inversedBy="socialInstitutions")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="intervention_area_id", referencedColumnName="id", nullable=false)
     */
    private $interventionArea;

    /**
     * @ORM\OneToMany(targetEntity="SocialInstitutionSocialResponse", mappedBy="interventionArea", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinTable(name="social_institution_area_response",
     *      joinColumns={@ORM\JoinColumn(name="intervention_area_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="social_response_id", referencedColumnName="id")}
     * )
     */
    private $socialResponses;

    public function __construct()
    {
        $this->socialResponses = new ArrayCollection();
    }

    /**
     * Set SocialInstitution
     *
     * @param SocialInstitution $socialInstitution
     *
     * @return SocialResponse
     */
    public function setSocialInstitution(SocialInstitution $socialInstitution = null)
    {
        $this->socialInstitution = $socialInstitution;
        return $this;
    }

    /**
     * Get SocialInstitution
     *
     * @return \AppBundle\Entity\SocialInstitution
     */
    public function getSocialInstitution()
    {
        return $this->socialInstitution;
    }

    /**
     * Gets the value of interventionArea.
     *
     * @return mixed
     */
    public function getInterventionArea()
    {
        return $this->interventionArea;
    }

    /**
     * Sets the value of interventionArea.
     *
     * @param mixed $interventionArea the intervention area
     *
     * @return self
     */
    public function setInterventionArea($interventionArea)
    {
        $this->interventionArea = $interventionArea;

        return $this;
    }

    /**
     * Add socialResponses
     *
     * @param SocialInstitutionSocialResponse $socialResponses
     * @return Race
     */
    public function addSocialResponse(SocialInstitutionSocialResponse $socialResponse)
    {
        $this->socialResponses[] = $socialResponse;
        $socialResponse->setInterventionArea($this);
        return $this;
    }

    /**
     * Remove socialResponse
     *
     * @param SocialInstitutionSocialResponse $socialResponse
     */
    public function removeSocialResponse(SocialInstitutionSocialResponse $socialResponse)
    {
        $this->socialResponses->removeElement($socialResponse);
    }

    /**
     * Get socialResponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialResponses()
    {
        return $this->socialResponses;
    }
}
