<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Expense
 *
 * @ORM\Table(name="expense")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExpenseRepository")
 *
 * @UniqueEntity(fields="name", message="This value is already in use, choose other.")
 * @UniqueEntity(fields="code", message="This value is already in use, choose other.")
 */
class Expense extends EntityAbstract
{
    use EntityAudit;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ExpenseReference
     *
     * @ORM\ManyToOne(targetEntity="ExpenseReference")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="expense_reference_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $expenseReference;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200)
     * 
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "max_chars"
     * )     
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=200, unique=true)
     * 
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "max_chars"
     * )
     */
    private $code;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 1;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Program", mappedBy="expense")
     */
    private $program;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Expense")
     * @ORM\JoinTable(name="relationship_expense",
     *   joinColumns={
     *     @ORM\JoinColumn(name="expense_source_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="expense_target_id", referencedColumnName="id")
     *   }
     * )
     */
    private $expenseTarget;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * 
     * @ORM\OneToMany(targetEntity="Expense", mappedBy="parent")
     */
    private $children;

    /**
     * @var Expense
     * 
     * @ORM\ManyToOne(targetEntity="Expense", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->program       = new ArrayCollection();
        $this->expenseTarget = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expenseReference
     *
     * @param ExpenseReference $expenseReference
     * @return Expense
     */
    public function setExpenseReference(ExpenseReference $expenseReference = null)
    {
        $this->expenseReference = $expenseReference;
    
        return $this;
    }
    
    /**
     * Get expenseReference
     *
     * @return ExpenseReference
     */
    public function getExpenseReference()
    {
        return $this->expenseReference;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Expense
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Expense
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set status
     * 
     * @param integer $status
     * @return Expense
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }
    
    /**
     * Get status
     * 
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add program
     *
     * @param Program $program
     * @return Expense
     */
    public function addProgram(Program $program)
    {
        $this->program[] = $program;
    
        return $this;
    }
    
    /**
     * Remove program
     *
     * @param Program $program
     */
    public function removeProgram(Program $program)
    {
        $this->program->removeElement($program);
    }
    
    /**
     * Get program
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Add expenseTarget
     *
     * @param Expense $expenseTarget
     * @return Expense
     */
    public function addExpenseTarget(Expense $expenseTarget)
    {
        $this->expenseTarget[] = $expenseTarget;

        return $this;
    }

    /**
     * Remove expenseTarget
     *
     * @param Expense $expenseTarget
     */
    public function removeExpenseTarget(Expense $expenseTarget)
    {
        $this->expenseTarget->removeElement($expenseTarget);
    }

    /**
     * Get expenseTarget
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpenseTarget()
    {
        return $this->expenseTarget;
    }
    
    /**
     * Set parent
     *
     * @param Expense $parent
     * @return Expense
     */
    public function setParent(Expense $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }
    
    /**
     * Get parent
     *
     * @return Expense
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    public function getChildren() {
        return $this->children;
    }
    
    /**
     * 
     * @return string
     */
    public function __toString()
    {
        return sprintf("%s - %s", $this->getCode(), $this->getName());
    }
}
