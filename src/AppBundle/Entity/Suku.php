<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Suku
 *
 * @ORM\Table(name="suku")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SukuRepository")
 */
class Suku extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PostoAdministrativo", inversedBy="sukus")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="posto_administrativo_id", referencedColumnName="id", nullable=false)
     */
    private $postoAdministrativo;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Suku
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getPostoAdministrativo()
    {
        return $this->postoAdministrativo;
    }
}
