<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BudgetBeneficiary
 *
 * @ORM\Table(name="budget_beneficiary")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BudgetBeneficiaryRepository")
 */
class BudgetBeneficiary extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="man", type="smallint")
     */
    private $man = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="woman", type="smallint")
     */
    private $woman = '0';

    /**
     * @var InterventionArea
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\InterventionArea")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="intervention_area_id", referencedColumnName="id")
     * })
     */
    private $interventionArea;

    /**
     * @var SocialResponse
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SocialResponse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="social_response_id", referencedColumnName="id")
     * })
     */
    private $socialResponse;

    /**
     * @var Budget
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Budget")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="budget_id", referencedColumnName="id")
     * })
     */
    private $budget;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set man
     *
     * @param integer $man
     *
     * @return BudgetBeneficiary
     */
    public function setMan($man)
    {
        $this->man = $man;

        return $this;
    }

    /**
     * Get man
     *
     * @return integer
     */
    public function getMan()
    {
        return $this->man;
    }

    /**
     * Set woman
     *
     * @param integer $woman
     *
     * @return BudgetBeneficiary
     */
    public function setWoman($woman)
    {
        $this->woman = $woman;

        return $this;
    }

    /**
     * Get woman
     *
     * @return integer
     */
    public function getWoman()
    {
        return $this->woman;
    }

    /**
     * Set interventionArea
     *
     * @param InterventionArea $interventionArea
     *
     * @return BudgetBeneficiary
     */
    public function setInterventionArea(InterventionArea $interventionArea = null)
    {
        $this->interventionArea = $interventionArea;

        return $this;
    }

    /**
     * Get interventionArea
     *
     * @return InterventionArea
     */
    public function getInterventionArea()
    {
        return $this->interventionArea;
    }

    /**
     * Set socialResponse
     *
     * @param SocialResponse $socialResponse
     *
     * @return BudgetBeneficiary
     */
    public function setSocialResponse(SocialResponse $socialResponse = null)
    {
        $this->socialResponse = $socialResponse;

        return $this;
    }

    /**
     * Get socialResponse
     *
     * @return SocialResponse
     */
    public function getSocialResponse()
    {
        return $this->socialResponse;
    }

    /**
     * Set budget
     *
     * @param Budget $budget
     *
     * @return BudgetBeneficiary
     */
    public function setBudget(Budget $budget = null)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return Budget
     */
    public function getBudget()
    {
        return $this->budget;
    }
}

