<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * SocialInstitutionContact
 *
 * @ORM\Table(name="social_institution_contact", indexes={@ORM\Index(name="fk_social_institution_contact_post1_idx", columns={"post_id"}), @ORM\Index(name="fk_social_institution_contact_social_institution1_idx", columns={"social_institution_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SocialInstitutionContact")
 */
class SocialInstitutionContact extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=45, nullable=true)
     * @Assert\Length(
     *      max = 45,
     *      maxMessage = "max_chars"
     * )
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email(message = "invalid_email")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     */
    private $email;

    /**
     * @var \AppBundle\Entity\Post
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Post")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="post_id", referencedColumnName="id")
     * })
     */
    private $post;

    /**
     * @var \AppBundle\Entity\SocialInstitution
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SocialInstitution", inversedBy="contacts")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="social_institution_id", referencedColumnName="id", nullable=false)
     * })
     * @JMS\Exclude()
     */
    private $socialInstitution;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SocialInstitutionContact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return SocialInstitutionContact
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return SocialInstitutionContact
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return SocialInstitutionContact
     */
    public function setPost(\AppBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \AppBundle\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set socialInstitution
     *
     * @param \AppBundle\Entity\SocialInstitution $socialInstitution
     *
     * @return SocialInstitutionContact
     */
    public function setSocialInstitution(\AppBundle\Entity\SocialInstitution $socialInstitution = null)
    {
        $this->socialInstitution = $socialInstitution;

        return $this;
    }

    /**
     * Get socialInstitution
     *
     * @return \AppBundle\Entity\SocialInstitution
     */
    public function getSocialInstitution()
    {
        return $this->socialInstitution;
    }
}
