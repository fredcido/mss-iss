<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GeneralRevenueSource
 *
 * @ORM\Table(name="general_revenue_source")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GeneralRevenueSourceRepository")
 *
 * @UniqueEntity(fields="name", message="This value is already in use, choose other.")
 */
class GeneralRevenueSource extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, unique=true)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "max_chars"
     * )     
     */
    private $name;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 1;

    /**
     * @ORM\OneToMany(targetEntity="RevenueSource", mappedBy="generalRevenueSource")
     */
    private $revenueSources;

    public function __construct()
    {
        $this->revenueSources = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GeneralIncomeSource
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add revenueSources
     *
     * @param RevenueSource $revenueSources
     * @return Race
     */
    public function addRevenueSource(RevenueSource $revenueSource)
    {
        $this->revenueSources[] = $revenueSources;
        return $this;
    }

    /**
     * Remove revenueSource
     *
     * @param RevenueSource $revenueSource
     */
    public function removeRevenueSource(RevenueSource $revenueSource)
    {
        $this->revenueSources->removeElement($revenueSources);
    }

    /**
     * Get revenueSources
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRevenueSources()
    {
        return $this->revenueSources;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return \AppBundle\Entity\Program
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }
    
    /**
     * Get status
     *
     * @return \AppBundle\Entity\smallint
     */
    public function getStatus()
    {
        return $this->status;
    }
}
