<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Intervention Area
 *
 * @ORM\Table(name="intervention_area")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InterventionAreaRepository")
 *
 * @UniqueEntity(fields="name", message="This value is already in use, choose other.")
 */
class InterventionArea extends EntityAbstract
{
    use EntityAudit;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, unique=true)
     * 
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "min_chars",
     *      maxMessage = "max_chars"
     * )     
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 1;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="SocialResponse", inversedBy="interventionArea")
     * @ORM\JoinTable(name="intervention_area_social_response",
     *   joinColumns={
     *     @ORM\JoinColumn(name="intervention_area_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="social_response_id", referencedColumnName="id")
     *   }
     * )
     */
    private $socialResponse;

    /**
     * @ORM\OneToMany(targetEntity="SocialInstitutionInterventionArea", mappedBy="interventionArea")
     */
    private $socialInstitutions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->socialResponse     = new ArrayCollection();
        $this->socialInstitutions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InterventionArea
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return InterventionArea
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add socialResponse
     *
     * @param SocialResponse $socialResponse
     * @return InterventionArea
     */
    public function addSocialResponse(SocialResponse $socialResponse)
    {
        $this->socialResponse[] = $socialResponse;
        
        return $this;
    }

    /**
     * Remove socialResponse
     *
     * @param SocialResponse $socialResponse
     * @return InterventionArea
     */
    public function removeSocialResponse(SocialResponse $socialResponse)
    {
        $this->socialResponse->removeElement($socialResponse);
    }

    /**
     * Get socialResponse
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialResponse()
    {
        return $this->socialResponse;
    }

    /**
     * Add socialInstitutions
     *
     * @param SocialInstitutionInterventionArea $socialInstitutions
     * @return Race
     */
    public function addSocialInstitution(SocialInstitutionInterventionArea $socialInstitution)
    {
        $this->socialInstitutions[] = $socialInstitution;
        
        return $this;
    }

    /**
     * Remove socialInstitution
     *
     * @param SocialInstitutionInterventionArea $socialInstitution
     */
    public function removeSocialInstitution(SocialInstitutionInterventionArea $socialInstitution)
    {
        $this->socialInstitutions->removeElement($socialInstitution);
    }

    /**
     * Get socialInstitutions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialInstitutions()
    {
        return $this->socialInstitutions;
    }
}
