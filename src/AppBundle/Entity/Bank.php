<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityAudit;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Bank
 *
 * @ORM\Table(name="bank")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BankRepository")
 */
class Bank extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, unique=true)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "max_chars"
     * )     
     */
    private $name;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 1;

    /**
     * @ORM\OneToMany(targetEntity="BankInformation", mappedBy="bank")
     */
    private $bankInformation;

    public function __construct()
    {
        $this->bankInformation = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bank
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of status.
     *
     * @return smallint
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the value of status.
     *
     * @param smallint $status the status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Add bankInformation
     *
     * @param BankInformation $bankInformation
     * @return Race
     */
    public function addBankInformation(BankInformation $bankInformation)
    {
        $this->bankInformation[] = $bankInformation;
        return $this;
    }

    /**
     * Remove bankInformation
     *
     * @param BankInformation $bankInformation
     */
    public function removeBankInformation(BankInformation $bankInformation)
    {
        $this->bankInformation->removeElement($bankInformation);
    }

    /**
     * Get bankInformation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBankInformations()
    {
        return $this->bankInformation;
    }
}
