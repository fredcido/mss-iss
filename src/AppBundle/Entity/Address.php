<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Address
 *
 * @ORM\Table(name="address", indexes={@ORM\Index(name="fk_address_suku1_idx", columns={"suku_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Address")
 */
class Address extends EntityAbstract
{
    use EntityAudit;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )     
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=45, nullable=true)
     * @Assert\Length(
     *      max = 45,
     *      maxMessage = "max_chars"
     * )     
     */
    private $postalCode;

    /**
     * @var \AppBundle\Entity\Suku
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Suku")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="suku_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $suku;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Address
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return Address
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set suku
     *
     * @param \AppBundle\Entity\Suku $suku
     *
     * @return Address
     */
    public function setSuku(Suku $suku = null)
    {
        $this->suku = $suku;

        return $this;
    }

    /**
     * Get suku
     *
     * @return \AppBundle\Entity\Suku
     */
    public function getSuku()
    {
        return $this->suku;
    }
}
