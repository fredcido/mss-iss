<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SocialResponse
 *
 * @ORM\Table(name="social_response")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SocialResponseRepository")
 * 
 * @UniqueEntity(fields="name", message="This value is already in use, choose other.")
 */
class SocialResponse extends EntityAbstract
{
    use EntityAudit;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, unique=true)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "max_chars"
     * )     
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 1;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="InterventionArea", mappedBy="socialResponse")
     */
    private $interventionArea;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->interventionArea = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SocialResponse
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status 
     * 
     * @param integer $status
     * @return SocialResponse
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status 
     * 
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Add interventionArea
     *
     * @param InterventionArea $interventionArea
     * @return SocialResponse
     */
    public function addInterventionArea(InterventionArea $interventionArea)
    {
        $this->interventionArea[] = $interventionArea;
         
        return $this;
    }
    
    /**
     * Remove interventionArea
     *
     * @param InterventionArea $interventionArea
     * @return SocialResponse
     */
    public function removeInterventionArea(InterventionArea $interventionArea)
    {
        $this->interventionAre->removeElement($interventionArea);
    }
    
    /**
     * Get interventionArea
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInterventionArea()
    {
        return $this->interventionArea;
    }
}