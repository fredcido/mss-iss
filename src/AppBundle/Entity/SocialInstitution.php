<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Contact;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SocialInstitution
 *
 * @ORM\Table(name="social_institution")})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SocialInstitutionRepository")
 */
class SocialInstitution extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     */
    private $name;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email(message = "invalid_email")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     * @Assert\Url(message = "invalid_url")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=true)
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "max_chars"
     * )
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity="SocialInstitutionContact", mappedBy="socialInstitution", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var \AppBundle\Entity\Contact
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity="Description", mappedBy="socialInstitution", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var  ArrayCollection
     */
    private $descriptions;

    /**
     * @ORM\OneToMany(targetEntity="BankInformation", mappedBy="socialInstitution", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var  ArrayyCollection
     */
    private $bankInformation;

    /**
     * @ORM\OneToMany(targetEntity="SocialInstitutionInterventionArea", mappedBy="socialInstitution", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var  ArrayCollection
     */
    private $interventionAreas;

    /**
     * @ORM\OneToOne(targetEntity="Address", cascade={"persist"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     *
     * @var Address
     */
    private $address;

      /**
       * @ORM\ManyToOne(targetEntity="InstitutionStatus")
       * @Assert\NotBlank(message = "not_blank")
       * @ORM\JoinColumn(name="institution_status_id", referencedColumnName="id", nullable=false)
       */
    private $institutionStatus;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->interventionAreas = new ArrayCollection();
        $this->descriptions = new ArrayCollection();
        $this->bankInformation = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SocialInstitution
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return \AppBundle\Entity\Program
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\smallint
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return SocialInstitution
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return SocialInstitution
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param SocialInstitutionContact $contact
     */
    public function addContact(SocialInstitutionContact $contact)
    {
        $this->contacts[] = $contact;
        $contact->setSocialInstitution($this);
        return $this;
    }

    /**
     * Remove descriptionType
     *
     * @param SocialInstitutionContact $descriptionType
     */
    public function removeContact(SocialInstitutionContact $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get descriptionType
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    public function addBankInformation(\AppBundle\Entity\BankInformation $bankInformation)
    {
        $this->bankInformation[] = $bankInformation;
        $bankInformation->setSocialInstitution($this);
        return $this;
    }

    /**
     * Remove descriptionType
     *
     * @param \AppBundle\Entity\BankInformation $bankInformation
     */
    public function removeBankInformation(\AppBundle\Entity\BankInformation $bankInformation)
    {
        $this->bankInformation->removeElement($bankInformation);
    }

    /**
     * Get descriptionType
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBankInformation()
    {
        return $this->bankInformation;
    }

    /**
     * Add interventionAreas
     *
     * @param InterventionArea $interventionAreas
     * @return Race
     */
    public function addInterventionArea(SocialInstitutionInterventionArea $interventionArea)
    {
        $this->interventionAreas[] = $interventionArea;
        $interventionArea->setSocialInstitution($this);
        return $this;
    }

    /**
     * Remove interventionArea
     *
     * @param InterventionArea $interventionArea
     */
    public function removeInterventionArea(SocialInstitutionInterventionArea $interventionArea)
    {
        $this->interventionAreas->removeElement($interventionArea);
    }

    /**
     * Get interventionAreas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInterventionAreas()
    {
        return $this->interventionAreas;
    }

     /**
     * Add descriptions
     *
     * @param Description $descriptions
     * @return Race
     */
    public function addDescription(Description $description)
    {
        $this->descriptions[] = $description;
        $description->setSocialInstitution($this);
        return $this;
    }

    /**
     * Remove description
     *
     * @param Description $description
     */
    public function removeDescription(Description $description)
    {
        $this->descriptions->removeElement($description);
    }

    /**
     * Get descriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * Gets the value of phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the value of phone.
     *
     * @param string $phone the phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Gets the value of address.
     *
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the value of address.
     *
     * @param Address $address the address
     *
     * @return self
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Gets the value of InstitutionStatus.
     *
     * @return InstitutionStatus
     */
    public function getInstitutionStatus()
    {
        return $this->institutionStatus;
    }

    /**
     * Sets the value of InstitutionStatus.
     *
     * @param InstitutionStatus $institutuinStatus the address
     *
     * @return self
     */
    public function setInstitutionStatus(InstitutionStatus $institutionStatus)
    {
        $this->institutionStatus = $institutionStatus;

        return $this;
    }
}
