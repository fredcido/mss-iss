<?php

namespace AppBundle\Entity;

abstract class EntityAbstract
{
    public function getEnum($property)
    {
        $class = $this->getEnumClass($property);
        
        $value = $this->getValueProperty($property);
        
        return call_user_func_array([$class, 'get'], [$value]);
    }
    
    /**
     *
     * @param string $property
     * @throws \InvalidArgumentException
     * @return string
     */
    private function getEnumClass($property)
    {
        $class = '\AppBundle\Enum\\' . implode(array_map('ucfirst', explode('_', $property)));
        
        if (! class_exists($class)) {
            throw new \InvalidArgumentException(sprintf(
                'Enum type %s is not defined',
                $class
            ));
        }
        
        return $class;
    }

    /**
     * 
     * @param string $property
     * @throws \InvalidargumentException
     * @return mixed
     */
    private function getValueProperty($property)
    {
        $method = 'get' . implode(array_map('ucfirst', explode('_', $property)));
        
        if (! method_exists($this, $method)) {
            throw new \InvalidargumentException(sprintf(
                'Method %s is not defined',
                $method
            ));
        }
        
        return call_user_func([$this, $method]);
    }
}