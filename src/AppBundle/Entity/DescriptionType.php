<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * DescriptionType
 *
 * @ORM\Table(name="description_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DescriptionTypeRepository")
 */
class DescriptionType extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "max_chars"
     * )
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Description", mappedBy="descriptionType")
     */
    private $descriptions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->descriptions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DescriptionType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add descriptions
     *
     * @param Description $descriptions
     * @return Race
     */
    public function addDescription(Description $description)
    {
        $this->descriptions[] = $descriptions;
        return $this;
    }

    /**
     * Remove description
     *
     * @param Description $description
     */
    public function removeDescription(Description $description)
    {
        $this->descriptions->removeElement($descriptions);
    }

    /**
     * Get descriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }
}
