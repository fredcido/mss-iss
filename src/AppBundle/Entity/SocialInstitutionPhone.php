<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SocialInstitutionPhone
 *
 * @ORM\Table(name="social_insitution_phone", indexes={@ORM\Index(name="fk_social_insitution_phone_social_institution1_idx", columns={"social_institution_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SocialInstitutionPhone")
 */
class SocialInstitutionPhone extends EntityAbstract
{
    use EntityAudit;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=45, nullable=true)
     * @Assert\Length(
     *      max = 45,
     *      maxMessage = "max_chars"
     * )     
     */
    private $number;

    /**
     * @var \AppBundle\Entity\SocialInstitution
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SocialInstitution")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="social_institution_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $socialInstitution;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return SocialInstitutionPhone
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set socialInstitution
     *
     * @param \AppBundle\Entity\SocialInstitution $socialInstitution
     *
     * @return SocialInstitutionPhone
     */
    public function setSocialInstitution(\AppBundle\Entity\SocialInstitution $socialInstitution = null)
    {
        $this->socialInstitution = $socialInstitution;

        return $this;
    }

    /**
     * Get socialInstitution
     *
     * @return \AppBundle\Entity\SocialInstitution
     */
    public function getSocialInstitution()
    {
        return $this->socialInstitution;
    }
}

