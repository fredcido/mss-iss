<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ExpenseType
 *
 * @ORM\Table(name="program")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProgramRepository")
 *
 * @UniqueEntity(fields="name", message="This value is already in use, choose other.")
 */
class Program extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, unique=true)
     * 
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "max_chars"
     * )     
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = 1;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Expense", inversedBy="program")
     * @ORM\JoinTable(name="program_expense",
     *   joinColumns={
     *     @ORM\JoinColumn(name="program_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="expense_id", referencedColumnName="id")
     *   }
     * )
     */
    private $expense;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->expense = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Program
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     * 
     * @param integer $status
     * @return Program
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }
    
    /**
     * Get status
     * 
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add expense
     *
     * @param Expense $expense
     * @return Program
     */
    public function addExpense(Expense $expense)
    {
        $this->expense[] = $expense;
        
        return $this;
    }

    /**
     * Remove expense
     *
     * @param Expense $expense
     */
    public function removeExpense(Expense $expense)
    {
        $this->expense->removeElement($expense);
    }

    /**
     * Get expense
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpenses()
    {
        return $this->expense;
    }
}