<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * DescriptionType
 *
 * @ORM\Table(name="description")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DescriptionRepository")
 */
class Description extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     * @Assert\NotBlank(message = "not_blank")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="DescriptionType", inversedBy="descriptions")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="description_type_id", referencedColumnName="id", nullable=false)
     */
    private $descriptionType;

    /**
     * @ORM\ManyToOne(targetEntity="SocialInstitution", inversedBy="descriptions")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="social_institution_id", referencedColumnName="id", nullable=false)
     * @JMS\Exclude()
     */
    private $socialInstitution;


    /**
     * Gets the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @param integer $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the value of description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the value of description.
     *
     * @param string $description the description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Gets the value of descriptionType.
     *
     * @return mixed
     */
    public function getDescriptionType()
    {
        return $this->descriptionType;
    }

    /**
     * Sets the value of descriptionType.
     *
     * @param mixed $descriptionType the description type
     *
     * @return self
     */
    public function setDescriptionType($descriptionType)
    {
        $this->descriptionType = $descriptionType;

        return $this;
    }

    /**
     * Gets the value of socialInstitution.
     *
     * @return mixed
     */
    public function getSocialInstitution()
    {
        return $this->socialInstitution;
    }

    /**
     * Sets the value of socialInstitution.
     *
     * @param mixed $socialInstitution the social institution
     *
     * @return self
     */
    public function setSocialInstitution($socialInstitution)
    {
        $this->socialInstitution = $socialInstitution;

        return $this;
    }
}
