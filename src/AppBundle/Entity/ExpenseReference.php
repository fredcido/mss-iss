<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExpenseReference
 *
 * @ORM\Table(name="expense_reference")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExpenseReferenceRepository")
 */
class ExpenseReference extends EntityAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=false, unique=true)
     */
    private $name;

    /**
     * @var smallint
     *
     * @ORM\Column(name="flag", type="smallint", nullable=false)
     */
    private $flag = 0;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ExpenseReference
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set flag
     *
     * @param smallint $flag
     *
     * @return ExpenseReference
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return smallint
     */
    public function getFlag()
    {
        return $this->flag;
    }
    
    public function __toString()
    {
        return (string) $this->getName();
    }
}

