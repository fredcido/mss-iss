<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    use EntityAudit;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=2)
     */
    protected $defaultLocale;

    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Returns the default locale
    */
    public function getDefaultLocale() {
        return $this->defaultLocale;
    }

    /**
    * Set the default locale
    */
    public function setDefaultLocale($locale) {
        $this->defaultLocale = $locale;
    }
}
