<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SocialInstitutionSocialResponse
 *
 * @ORM\Table(name="social_institution_social_response")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SocialInstitutionSocialResponseRepository")
 */
class SocialInstitutionSocialResponse extends EntityAbstract
{
    use EntityAudit;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SocialResponse")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="social_response_id", referencedColumnName="id", nullable=false)
     */
    private $socialResponse;

    /**
     * @var int
     *
     * @ORM\Column(name="support_mss", type="smallint")
     */
    private $supportMss = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="percentage", type="float")
     */
    private $percentage = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="place_ownership", type="smallint")
     */
    private $placeOwnership;

    /**
     * @ORM\OneToOne(targetEntity="Address", cascade={"persist"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     *
     * @var Address
     */
    private $address;

    /**
     * @ORM\ManyToMany(targetEntity="Municipio")
     * @ORM\JoinTable(name="social_response_district",
     *      joinColumns={@ORM\JoinColumn(name="social_response_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="municipio_id", referencedColumnName="id")}
     * )
     */
    private $districts;

    /**
     * @var int
     *
     * @ORM\Column(name="men", type="smallint")
     */
    private $men = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="women", type="smallint")
     */
    private $women = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="total", type="smallint")
     */
    private $total = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text")
     */
    private $observation;

    /**
     * @var \AppBundle\Entity\SocialInstitutionInterventionArea
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SocialInstitutionInterventionArea", inversedBy="socialResponses")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="intervention_area_id", referencedColumnName="id", nullable=false)
     * })
     * @JMS\Exclude()
     */
    private $interventionArea;


    public function __construct()
    {
        $this->districts = new ArrayCollection();
    }


    /**
     * Set socialResponse
     *
     * @param \AppBundle\Entity\SocialResponse $socialResponse
     *
     * @return Municipio
     */
    public function setSocialResponse(SocialResponse $socialResponse = null)
    {
        $this->socialResponse = $socialResponse;

        return $this;
    }

    /**
     * Gets the value of supportMss.
     *
     * @return int
     */
    public function getSupportMss()
    {
        return $this->supportMss;
    }

    /**
     * Sets the value of supportMss.
     *
     * @param int $supportMss the support mss
     *
     * @return self
     */
    public function setSupportMss($supportMss)
    {
        $this->supportMss = $supportMss;
        return $this;
    }

    /**
     * Gets the value of percentage.
     *
     * @return float
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Sets the value of percentage.
     *
     * @param float $percentage the percentage
     *
     * @return self
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
        return $this;
    }

    /**
     * Gets the value of placeOwnership.
     *
     * @return int
     */
    public function getPlaceOwnership()
    {
        return $this->placeOwnership;
    }

    /**
     * Sets the value of placeOwnership.
     *
     * @param int $placeOwnership the place ownership
     *
     * @return self
     */
    public function setPlaceOwnership($placeOwnership)
    {
        $this->placeOwnership = $placeOwnership;
        return $this;
    }

    /**
     * Remove district
     *
     * @param Municipio $district
     */
    public function removeMunicipio(Municipio $district)
    {
        $this->districts->removeElement($districts);
    }

    /**
     * Get districts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMunicipios()
    {
        return $this->districts;
    }

    /**
     * Gets the value of men.
     *
     * @return int
     */
    public function getMen()
    {
        return $this->men;
    }

    /**
     * Sets the value of men.
     *
     * @param int $men the men
     *
     * @return self
     */
    public function setMen($men)
    {
        $this->men = $men;
        $this->calcTotal();

        return $this;
    }

    protected function calcTotal()
    {
        $total = (int)$this->getMen() + (int)$this->getWomen();
        $this->total = $total;
    }

    /**
     * Gets the value of women.
     *
     * @return int
     */
    public function getWomen()
    {
        return $this->women;
    }

    /**
     * Sets the value of women.
     *
     * @param int $women the women
     *
     * @return self
     */
    public function setWomen($women)
    {
        $this->women = $women;
        $this->calcTotal();
        return $this;
    }

    /**
     * Gets the value of total.
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Sets the value of total.
     *
     * @param int $total the total
     *
     * @return self
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Gets the value of observation.
     *
     * @return stringg
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Sets the value of observation.
     *
     * @param string $observation the observation
     *
     * @return self
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Gets the value of address.
     *
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the value of address.
     *
     * @param Address $address the address
     *
     * @return self
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Gets the }).
     *
     * @return \AppBundle\Entity\SocialInstitutionInterventionArea
     */
    public function getInterventionArea()
    {
        return $this->interventionArea;
    }

    /**
     * Sets the }).
     *
     * @param SocialInstitutionInterventionArea $interventionArea the intervention area
     *
     * @return self
     */
    public function setInterventionArea(SocialInstitutionInterventionArea $interventionArea)
    {
        $this->interventionArea = $interventionArea;

        return $this;
    }

    /**
     * Gets the value of socialResponse.
     *
     * @return mixed
     */
    public function getSocialResponse()
    {
        return $this->socialResponse;
    }

    /**
     * Add districts
     *
     * @param Municipio $districts
     * @return Race
     */
    public function addDistrict(Municipio $district)
    {
        $this->districts[] = $district;
        return $this;
    }

    /**
     * Remove district
     *
     * @param Municipio $district
     */
    public function removeDistrict(Municipio $district)
    {
        $this->districts->removeElement($district);
    }

    /**
     * Get districts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDistricts()
    {
        return $this->districts;
    }
}
