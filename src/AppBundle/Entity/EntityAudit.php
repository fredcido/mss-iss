<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 */
trait EntityAudit
{
    /**
     * @var \DateTime 
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */    
    protected $created_at;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="created_by", type="integer", nullable=true)
     */
    protected $created_by;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updated_at;
    
    /**
     * @var int
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true)
     */
    protected $updated_by;
    
    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return \AppBundle\Entity\EntityAudit
     */
    public function setCreatedAt(\DateTime $created_at)
    {
        $this->created_at = $created_at;
         
        return $this;
    }
     
    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * Set created_by
     *
     * @param int $created_by
     * @return \AppBundle\Entity\EntityAudit
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    
        return $this;
    }
    
    /**
     * Get created_by
     *
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }
    
    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return \AppBundle\Entity\EntityAudit
     */
    public function setUpdatedAt(\DateTime $updated_at)
    {
        $this->updated_at = $updated_at;
         
        return $this;
    }
     
    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * Set updated_by
     *
     * @param int $updated_by
     * @return \AppBundle\Entity\EntityAudit
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    
        return $this;
    }
    
    /**
     * Get updated_by
     *
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }
}