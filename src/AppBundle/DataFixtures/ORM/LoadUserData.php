<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User;
use AppBundle\Entity\InstitutionStatus;
use AppBundle\AppBundle;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    public function load(ObjectManager $manager)
    {
        $doctrine = $this->container->get('doctrine');
        //$institutionStatusManager = $em->getRepository('AppBundle:InstitutionStatus');
        $userManager = $this->container->get('fos_user.user_manager');

        // Create our user and set details
        $user = $userManager->createUser();
        $user->setUsername('admin');
        $user->setEmail('admin@admin.com');
        $user->setPlainPassword('adm');
        $user->setDefaultLocale('pt');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_ADMIN'));

        // Update the user
        $userManager->updateUser($user, true);

        // Institution Status
        $em = $doctrine->getManager();

        $institutionStatus1 = new InstitutionStatus();
        $institutionStatus1->setName('Sem Estatuto de ISS');
        $em->persist($institutionStatus1);
        $em->flush();

        $institutionStatus2 = new InstitutionStatus();
        $institutionStatus2->setName('Estatuto de ISS');
        $em->persist($institutionStatus2);
        $em->flush();

        $institutionStatus3 = new InstitutionStatus();
        $institutionStatus3->setName('Cumpre Normas Técnicas');
        $em->persist($institutionStatus3);
        $em->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
