<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\InterventionArea;

class LoadInterventionArea implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $iaDeficiencia = new InterventionArea();
        $iaDeficiencia->setName("Deficiência");
        $manager->persist($iaDeficiencia);

        $manager->flush();
    }
}
