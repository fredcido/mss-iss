<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Role;

class LoadRoleData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $roleUser = new Role();
        $roleUser->setName('ROLE_USER');
        $roleUser->setDescription('Users');
        $roleUser->setComment('Default role for all users');
        $manager->persist($roleUser);

        $roleAdmin = new Role();
        $roleAdmin->setName('ROLE_ADMIN');
        $roleAdmin->setDescription('Admins');
        $roleAdmin->setComment('Role for admin users');
        $manager->persist($roleAdmin);

        $roleSuperAdmin = new Role();
        $roleSuperAdmin->setName('ROLE_SUPER_ADMIN');
        $roleSuperAdmin->setDescription('Super Admins');
        $roleSuperAdmin->setComment('Role form super admins. Full access to the system');
        $manager->persist($roleSuperAdmin);

        $manager->flush();
    }
}
