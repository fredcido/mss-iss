<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Setting;

class LoadSetting implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $setting1 = new Setting();
        $setting1->setName("min_cost");
        $setting1->setValue("1500");
        $setting1->setExplanation("This is the minimum cost accepted");
        $manager->persist($setting1);

        $setting2 = new Setting();
        $setting2->setName("max_cost");
        $setting2->setValue("3200");
        $setting2->setExplanation("This is the maximum cost accepted");
        $manager->persist($setting2);

        $setting3 = new Setting();
        $setting3->setName("max_beneficiaries");
        $setting3->setValue("500");
        $setting3->setExplanation("The max number of allowed beneficiaries");
        $manager->persist($setting3);

        $manager->flush();
    }
}
