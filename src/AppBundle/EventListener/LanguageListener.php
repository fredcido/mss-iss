<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class LanguageListener
{
    private $session;

    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    public function setLocale(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();
        $validLocales = array('en', 'pt', 'tl');
        $userLocale = $request->getPreferredLanguage($validLocales);

        if ($locale = $request->attributes->get('_locale')) {
            $defaultLocale = $locale;
        } else {
            $defaultLocale = $request->getSession()->get('_locale', $userLocale);
        }

        if (in_array($defaultLocale, $validLocales)) {
            $userLocale = $defaultLocale;
        }

        $request->getSession()->set('_locale', $userLocale);
        $request->setLocale($userLocale);
    }
}
