<?php

namespace AppBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 *
 */
class AuditEvent implements EventSubscriber
{
    use ContainerAwareTrait;

    /**
     * (non-PHPdoc)
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::onFlush];
    }

    /**
     *
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();

        $this->insertions($uow);
        $this->updates($uow);
    }

    /**
     * Get identify of the user
     *
     * @return int
     */
    private function getUserId()
    {
        if (null === ($token = $this->container->get('security.token_storage')->getToken())) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            return;
        }

        return $user->getId();
    }

    /**
     *
     * @param UnitOfWork $args
     */
    private function insertions(UnitOfWork $uow)
    {
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            $reflection = new \ReflectionClass($entity);

            if ($reflection->hasProperty('created_at')) {
                $now = new \DateTime('now');
                $uow->propertyChanged($entity, 'created_at', null, $now);
                $entity->setCreatedAt($now);
            }

            if ($reflection->hasProperty('created_by')) {
                $userId = $this->getUserId();
                $uow->propertyChanged($entity, 'created_by', null, $userId);
                $entity->setCreatedBy($userId);
            }
        }

    }

    /**
     *
     * @param OnFlushEventArgs $args
     */
    private function updates(UnitOfWork $uow)
    {
        foreach ($uow->getScheduledEntityUpdates() as $entity) {

            $reflection = new \ReflectionClass($entity);

            if ($reflection->hasProperty('updated_at')) {
                $now = new \DateTime('now');
                $uow->propertyChanged($entity, 'updated_at', null, $now);
                $entity->setCreatedAt($now);
            }

            if ($reflection->hasProperty('updated_by')) {
                $userId = $this->getUserId();
                $uow->propertyChanged($entity, 'updated_by', null, $userId);
                $entity->setCreatedBy($userId);
            }

        }
    }
}
