<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocialInstitutionInterventionAreaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'intervention_area',
            EntityType::class,
            array(
                'class'        => 'AppBundle:InterventionArea',
                'choice_label' => function ($e) {
                    return $e->getName();
                },
                'label'        => 'InterventionArea', 'translation_domain' => 'app',
            )
        );

        $builder->add(
            'social_responses',
            CollectionType::class,
            array(
                'entry_type'   => SocialInstitutionSocialResponseType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\SocialInstitutionInterventionArea',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_social_institution_intervention_area';
    }
}
