<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RevenueSourceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('general_revenue_source', EntityType::class, array(
            'class' => 'AppBundle:GeneralRevenueSource',
            'choice_label' => function ($e) {
                return $e->getName();
            },
            'label' => 'GeneralRevenueSource', 'translation_domain' => 'app'
        ));
        $builder->add('name', null, array('label' => 'name', 'translation_domain' => 'app'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\RevenueSource'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_revenuesource';
    }
}
