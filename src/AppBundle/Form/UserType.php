<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('defaultLocale', ChoiceType::class, array(
            'choices'  => array(
                'Portuguese' => 'pt',
                'English' => 'en',
                'Tetum' => 'tl',
            )
        ));

        $builder->add('roles', ChoiceType::class, array(
            'attr'         => array('class' => 'col-md-12'),
            'choices'  => array(
                'Users' => 'ROLE_USER',
                'Admins' => 'ROLE_ADMIN',
                'Super Admins' => 'ROLE_SUPER_ADMIN',
            ),
            'multiple'     => true,
            'expanded'     => true
        ));

        /*$builder->add('roles', EntityType::class, array(
            'class'        => 'AppBundle:Role',
            'attr'         => array('class' => 'col-sm-12'),
            'choice_label' => function ($e) {
                return $e->getDescription();
            },
            'choice_value' => 'name',
            'multiple'     => true,
            'expanded'     => true,
        ));*/

   }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }        
    
}
