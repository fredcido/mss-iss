<?php

namespace AppBundle\Form;

use AppBundle\Enum\PlaceOwnership;
use AppBundle\Enum\SupportMss;
use AppBundle\Form\AddressType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocialInstitutionSocialResponseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'social_response',
            EntityType::class,
            array(
                'class'              => 'AppBundle:SocialResponse',
                'choice_label'       => function ($e) {
                    return $e->getName();
                },
                'label'              => 'SocialResponse',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'supportMss',
            NumberType::class,
            array(
                'label'              => 'SupportMss',
                'translation_domain' => 'app',
            )
        );

        // $builder->add(
        //     'supportMss',
        //     ChoiceType::class,
        //     array(
        //         'choices'            => SupportMss::getOptions(),
        //         'label'              => 'SupportMss',
        //         'translation_domain' => 'app',
        //         'required'           => true,
        //     )
        // );

        $builder->add(
            'percentage',
            NumberType::class,
            array(
                'label'              => 'Percentage',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'placeOwnership',
            NumberType::class,
            array(
                'label'              => 'PlaceOwnership',
                'translation_domain' => 'app',
            )
        );

        // $builder->add(
        //     'placeOwnership',
        //     ChoiceType::class,
        //     array(
        //         'choices'            => PlaceOwnership::getOptions(),
        //         'label'              => 'PlaceOwnership',
        //         'translation_domain' => 'app',
        //         'required'           => true,
        //     )
        // );

        $builder->add(
            'address',
            AddressType::class
        );

        $builder->add(
            'districts',
            EntityType::class,
            array(
                'class'              => 'AppBundle:Municipio',
                'choice_label'       => function ($e) {
                    return $e->getName();
                },
                'multiple'           => true,
                'expanded'           => false,
                'label'              => 'Municipio',
                'translation_domain' => 'app',
                'attr' => array('class' => 'chosen')
            )
        );

        $builder->add(
            'men',
            IntegerType::class,
            array(
                'label'              => 'Men',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'women',
            IntegerType::class,
            array(
                'label'              => 'Women',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'total',
            IntegerType::class,
            array(
                'label'              => 'Total',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'observation',
            TextareaType::class,
            array(
                'label'              => 'Observation',
                'translation_domain' => 'app',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SocialInstitutionSocialResponse',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_social_institution_social_response';
    }
}
