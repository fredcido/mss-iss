<?php

namespace AppBundle\Form;

use AppBundle\Entity\InstitutionStatus;
use AppBundle\Form\AddressType;
use AppBundle\Form\SocialInstitutionDescriptionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocialInstitutionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            array(
                'label'              => 'name',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'email',
            EmailType::class,
            array(
                'label'              => 'email',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'website',
            TextType::class,
            array(
                'label'              => 'website',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'phone',
            TextType::class,
            array(
                'label'              => 'phone',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'address',
            AddressType::class
        );

        $builder->add(
            'institution_status',
            EntityType::class,
            array(
                'class'        => 'AppBundle:InstitutionStatus',
                'choice_label' => function ($e) {
                    return $e->getName();
                },
                'label'        => 'Status', 'translation_domain' => 'app',
            )
        );

        $builder->add(
            'descriptions',
            CollectionType::class,
            array(
                'entry_type'   => SocialInstitutionDescriptionType::class,
                'allow_add'    => true,
                'by_reference' => false,
            )
        );

        $builder->add(
            'contacts',
            CollectionType::class,
            array(
                'entry_type'   => SocialInstitutionContactType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            )
        );

        $builder->add(
            'bank_information',
            CollectionType::class,
            array(
                'entry_type'   => SocialInstitutionBankType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            )
        );

        $builder->add(
            'intervention_areas',
            CollectionType::class,
            array(
                'entry_type'   => SocialInstitutionInterventionAreaType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\SocialInstitution',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
