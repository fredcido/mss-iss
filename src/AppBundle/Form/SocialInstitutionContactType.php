<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocialInstitutionContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            array(
                'label'              => 'name',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'telephone',
            TextType::class,
            array(
                'label'              => 'telephone',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'email',
            EmailType::class,
            array(
                'label'              => 'email',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'post',
            EntityType::class,
            array(
                'class'        => 'AppBundle:Post',
                'choice_label' => function ($e) {
                    return $e->getName();
                },
                'label'        => 'Post', 'translation_domain' => 'app',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SocialInstitutionContact',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_social_institution_contact';
    }
}
