<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'address',
            TextType::class,
            array(
                'label'              => 'address',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'postal_code',
            TextType::class,
            array(
                'label'              => 'postal_code',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'suku',
            EntityType::class,
            array(
                'class'        => 'AppBundle:Suku',
                'choice_label' => function ($e) {
                    return $e->getName();
                },
                'label'        => 'Suku', 'translation_domain' => 'app',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\Address',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_address';
    }
}
