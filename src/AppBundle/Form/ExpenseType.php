<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpenseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('expense_reference', EntityType::class, [
            'class'              => 'AppBundle:ExpenseReference',
            'choice_label'       => function ($e) {
                return $e->getName();
            },
            'placeholder'        => '',
            'label'              => 'ExpenseReference',
            'translation_domain' => 'app',
            'attr'               => ['class' => 'expense-reference'],
        ]);

        $builder->add('code', null, array('label' => 'code', 'translation_domain' => 'app'));
        $builder->add('name', null, array('label' => 'name', 'translation_domain' => 'app'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\Expense',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_expense';
    }
}
