<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocialInstitutionBankType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            array(
                'label'              => 'name',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'nib',
            TextType::class,
            array(
                'label'              => 'nib',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'iban',
            TextType::class,
            array(
                'label'              => 'iban',
                'translation_domain' => 'app',
            )
        );

        $builder->add(
            'bank',
            EntityType::class,
            array(
                'class'        => 'AppBundle:Bank',
                'choice_label' => function ($e) {
                    return $e->getName();
                },
                'label'        => 'Bank', 'translation_domain' => 'app',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\BankInformation',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_bank_information';
    }
}
