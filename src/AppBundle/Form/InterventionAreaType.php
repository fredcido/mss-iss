<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InterventionAreaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, array('label' => 'name', 'translation_domain' => 'app'));
        $builder->add(
            'socialResponse',
            EntityType::class,
            array(
                'class'              => 'AppBundle:SocialResponse',
                'choice_label'       => function ($e) {
                    return $e->getName();
                },
                'required'           => false,
                'multiple'           => true,
                'expanded'           => false,
                'label'              => 'SocialResponse',
                'translation_domain' => 'app',
                'attr' => array('class' => 'chosen')
            )
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\InterventionArea'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_interventionarea';
    }
}
