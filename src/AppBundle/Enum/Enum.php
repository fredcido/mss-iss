<?php

namespace AppBundle\Enum;

use InvalidArgumentException;

/**
 *
 */
interface Enum
{
    /**
     * @param string $key
     * @throws InvalidArgumentException
     * @return mixed
     */
    public static function get($key = null);
}