<?php

namespace AppBundle\Enum;

/**
 *
 */
abstract class PlaceOwnership implements Enum
{
    const OWNED = 1;
    const LEASED = 2;

    /**
     * @var array
     */
    protected static $data = [
        self::OWNED  => 'Proprio',
        self::LEASED  => 'Alugado',
    ];

    public static function getOptions()
    {
        return self::$data;
    }

    /**
     *
     * @param string $key
     * @throws \InvalidArgumentException
     */
    public static function get($key = null)
    {
        if (null === $key) {
            return self::$data;
        }

        if (! in_array($key, array_keys(self::$data))) {
            throw new \InvalidArgumentException(sprintf(
                '%s is not defined',
                $key
            ));
        }

        return self::$data[$key];
    }
}
