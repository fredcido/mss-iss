<?php

namespace AppBundle\Enum;

/**
 *
 */
abstract class Status implements Enum
{
    const INACTIVE = 0;
    const ACTIVE = 1;
    
    /**
     * @var array
     */
    protected static $data = [
        self::INACTIVE  => 'Inactive',
        self::ACTIVE    => 'Active',
    ];

    public static function getOptions()
    {
        return self::$data;
    }

    /**
     *
     * @param string $key
     * @throws \InvalidArgumentException
     */
    public static function get($key = null)
    {
        if (null === $key) {
            return self::$data;
        }
        
        if (! in_array($key, array_keys(self::$data))) {
            throw new \InvalidArgumentException(sprintf(
                '%s is not defined',
                $key
            ));
        }
        
        return self::$data[$key];
    }
}
