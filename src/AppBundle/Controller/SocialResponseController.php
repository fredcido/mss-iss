<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SocialResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller.
 *
 * @Route("/social-response")
 * @Security("has_role('ROLE_ADMIN')")
 */
class SocialResponseController extends Controller
{
    /**
     * Lists all entities.
     *
     * @Route("/", name="social_response_index", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:SocialResponse:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:SocialResponse')->findAll();

        return array(
            'entities' => $entities,
            'status' => \AppBundle\Enum\Status::get(),
        );
    }

    /**
     * @Route("/status")
     * @Method("POST")
     */
    public function statusAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $collection = $em->getRepository('AppBundle:SocialResponse')->findBy([
                'id' => $request->get('identifiers', null),
            ]);

            foreach ($collection as $entity) {
                $entity->setStatus($request->get('status'));
                $em->persist($entity);
            }

            $em->flush();

            return new JsonResponse(true);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    /**
     * Displays a form to create a new entity.
     *
     * @Route("/new", name="social_response_new")
     * @Method("GET")
     * @Template("AppBundle:SocialResponse:form.html.twig")
     */
    public function newAction()
    {
        $entity = new SocialResponse();
        $form = $this->createPostForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing entity.
     *
     * @Route("/{id}/edit", name="social_response_edit")
     * @Method("GET")
     * @Template("AppBundle:SocialResponse:form.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:SocialResponse')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SocialResponse entity.');
        }

        $editForm = $this->createPostForm($entity, $id);

        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
        );
    }

    /**
     * Creates a new entity.
     *
     * @Route("/save/{id}", name="social_response_save", defaults={"id": null})
     * @Method("POST")
     * @Template("AppBundle:SocialResponse:form.html.twig")
     */
    public function saveAction(Request $request, $id)
    {
        if (!$id) {
            $entity = new SocialResponse();
        } else {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:SocialResponse')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SocialResponse entity.');
            }
        }

        $form = $this->createPostForm($entity, $id);
        $form->handleRequest($request);

        $messageBroker = $this->get('app.message.broker');
        $messageDefaults = $this->container->getParameter('messages');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $form = $this->createPostForm($entity, $entity->getId());

            $messageBroker->addSuccess($messageDefaults['success']);

            return $this->redirect($this->generateUrl('social_response_edit', ['id' => $entity->getId()]));
        } else {
            $messageBroker->addError($messageDefaults['error']);
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to save a entity.
     *
     * @param $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createPostForm(SocialResponse $entity, $id = null)
    {
        $form = $this->createForm('AppBundle\Form\SocialResponseType', $entity, array(
            'action' => $this->generateUrl('social_response_save', array('id' => $id)),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Creates a form to delete a entity.
     *
     *  @param $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm(SocialResponse $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('social_response_delete', array('id' => $entity->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a confirmation message to delete an existing entity.
     *
     * @Route("/{id}/delete", name="social_response_delete_confirm")
     * @Method({"GET"})
     */
    public function deleteConfirmAction(Request $request, SocialResponse $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('AppBundle:SocialResponse:delete.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a entity.
     *
     * @Route("/{id}", name="social_response_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:SocialResponse')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SocialResponse entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirectToRoute('social_response_index');
    }
}
