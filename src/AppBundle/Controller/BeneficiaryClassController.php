<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BeneficiaryClass;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * controller.
 *
 * @Route("/beneficiary-class")
 * @Security("has_role('ROLE_ADMIN')")
 */
class BeneficiaryClassController extends Controller
{
    /**
     * Lists all entities.
     *
     * @Route("/", name="beneficiary_class_index", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:BeneficiaryClass:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:BeneficiaryClass')->findAll();

        return array(
            'entities' => $entities,
            'status' => \AppBundle\Enum\Status::get(),
        );
    }

    /**
     * @Route("/status")
     * @Method("POST")
     */
    public function statusAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $collection = $em->getRepository('AppBundle:BeneficiaryClass')->findBy([
                'id' => $request->get('identifiers', null),
            ]);

            foreach ($collection as $entity) {
                $entity->setStatus($request->get('status'));
                $em->persist($entity);
            }

            $em->flush();

            return new JsonResponse(true);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    /**
     * Displays a form to create a new entity.
     *
     * @Route("/new", name="beneficiary_class_new")
     * @Method("GET")
     * @Template("AppBundle:BeneficiaryClass:form.html.twig")
     */
    public function newAction()
    {
        $entity = new BeneficiaryClass();
        $form = $this->createPostForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing entity.
     *
     * @Route("/{id}/edit", name="beneficiary_class_edit")
     * @Method("GET")
     * @Template("AppBundle:BeneficiaryClass:form.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:BeneficiaryClass')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BeneficiaryClass entity.');
        }

        $editForm = $this->createPostForm($entity, $id);

        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
        );
    }

    /**
     * Creates a new entity.
     *
     * @Route("/save/{id}", name="beneficiary_class_save", defaults={"id": null})
     * @Method("POST")
     * @Template("AppBundle:BeneficiaryClass:form.html.twig")
     */
    public function saveAction(Request $request, $id)
    {
        if (!$id) {
            $entity = new BeneficiaryClass();
        } else {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:BeneficiaryClass')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BeneficiaryClass entity.');
            }
        }

        $form = $this->createPostForm($entity, $id);
        $form->handleRequest($request);

        $messageBroker = $this->get('app.message.broker');
        $messageDefaults = $this->container->getParameter('messages');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $form = $this->createPostForm($entity, $entity->getId());
            $messageBroker->addSuccess($messageDefaults['success']);

            return $this->redirect($this->generateUrl('beneficiary_class_edit', ['id' => $entity->getId()]));
        } else {
            $messageBroker->addError($messageDefaults['error']);
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to save a entity.
     *
     * @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createPostForm(BeneficiaryClass $entity, $id = null)
    {
        $form = $this->createForm('AppBundle\Form\BeneficiaryClassType', $entity, array(
            'action' => $this->generateUrl('beneficiary_class_save', array('id' => $id)),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Creates a form to delete a entity.
     *
     *  @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm(BeneficiaryClass $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('beneficiary_class_delete', array('id' => $entity->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a confirmation message to delete an existing entity.
     *
     * @Route("/{id}/delete", name="beneficiary_class_delete_confirm")
     * @Method({"GET"})
     */
    public function deleteConfirmAction(Request $request, BeneficiaryClass $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('AppBundle:BeneficiaryClass:delete.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a entity.
     *
     * @Route("/{id}", name="beneficiary_class_delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:BeneficiaryClass')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BeneficiaryClass entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirectToRoute('beneficiary_class_index');
    }
}
