<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * controller.
 *
 * @Route("/post")
 * @Security("has_role('ROLE_ADMIN')")
 */
class PostController extends Controller
{
    /**
     * Lists all entities.
     *
     * @Route("/", name="post_index", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:Post:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:Post')->findAll();

        return array(
            'entities' => $entities,
            'status' => \AppBundle\Enum\Status::get(),
        );
    }

    /**
     * @Route("/status")
     * @Method("POST")
     */
    public function statusAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $collection = $em->getRepository('AppBundle:Post')->findBy([
                'id' => $request->get('identifiers', null),
            ]);

            foreach ($collection as $entity) {
                $entity->setStatus($request->get('status'));
                $em->persist($entity);
            }

            $em->flush();

            return new JsonResponse(true);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    /**
     * Displays a form to create a new entity.
     *
     * @Route("/new", name="post_new")
     * @Method("GET")
     * @Template("AppBundle:Post:form.html.twig")
     */
    public function newAction()
    {
        $entity = new Post();
        $form = $this->createPostForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing entity.
     *
     * @Route("/{id}/edit", name="post_edit")
     * @Method("GET")
     * @Template("AppBundle:Post:form.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Post')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Post entity.');
        }

        $editForm = $this->createPostForm($entity, $id);

        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
        );
    }

    /**
     * Creates a new entity.
     *
     * @Route("/save/{id}", name="post_save", defaults={"id": null})
     * @Method("POST")
     * @Template("AppBundle:Post:form.html.twig")
     */
    public function saveAction(Request $request, $id)
    {
        if (!$id) {
            $entity = new Post();
        } else {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Post')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Post entity.');
            }
        }

        $form = $this->createPostForm($entity, $id);
        $form->handleRequest($request);

        $messageBroker = $this->get('app.message.broker');
        $messageDefaults = $this->container->getParameter('messages');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $form = $this->createPostForm($entity, $entity->getId());

            $messageBroker->addSuccess($messageDefaults['success']);

            return $this->redirect($this->generateUrl('post_edit', ['id' => $entity->getId()]));
        } else {
            $messageBroker->addError($messageDefaults['error']);
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to save a entity.
     *
     * @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createPostForm(Post $entity, $id = null)
    {
        $form = $this->createForm('AppBundle\Form\PostType', $entity, array(
            'action' => $this->generateUrl('post_save', array('id' => $id)),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Creates a form to delete a entity.
     *
     *  @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm(Post $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('post_delete', array('id' => $entity->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a confirmation message to delete an existing entity.
     *
     * @Route("/{id}/delete", name="post_delete_confirm")
     * @Method({"GET"})
     */
    public function deleteConfirmAction(Request $request, Post $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('AppBundle:Post:delete.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a entity.
     *
     * @Route("/{id}", name="post_delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Post')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Post entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirectToRoute('post_index');
    }
}
