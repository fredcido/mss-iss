<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bank;
use AppBundle\Form\BankType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * controller.
 *
 * @Route("/bank")
 * @Security("has_role('ROLE_ADMIN')")
 */
class BankController extends Controller
{
    /**
     * Lists all entities.
     *
     * @Route("/", name="bank_index", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:Bank:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:Bank')->findAll();

        return array(
            'entities' => $entities,
            'status'   => \AppBundle\Enum\Status::get(),
        );
    }

    /**
     *
     * @Route("/status")
     * @Method("POST")
     */
    public function statusAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $collection = $em->getRepository('AppBundle:Bank')->findBy([
                'id' => $request->get('identifiers', null)
            ]);

            foreach ($collection as $entity) {
                $entity->setStatus($request->get('status'));
                $em->persist($entity);
            }

            $em->flush();

            return new JsonResponse(true);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    /**
     * Displays a form to create a new entity.
     *
     * @Route("/new", name="bank_new")
     * @Method("GET")
     * @Template("AppBundle:Bank:form.html.twig")
     */
    public function newAction()
    {
        $entity = new Bank();
        $form = $this->createBankForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing entity.
     *
     * @Route("/{id}/edit", name="bank_edit")
     * @Method("GET")
     * @Template("AppBundle:Bank:form.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Bank')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bank entity.');
        }

        $editForm = $this->createBankForm($entity, $id);

        return array(
            'entity' => $entity,
            'form'   => $editForm->createView(),
        );
    }

    /**
     * Creates a new entity.
     *
     * @Route("/save/{id}", name="bank_save", defaults={"id": null})
     * @Method("POST")
     * @Template("AppBundle:Bank:form.html.twig")
     */
    public function saveAction(Request $request, $id)
    {
        if (!$id) {
            $entity = new Bank();
        } else {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Bank')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Bank entity.');
            }
        }

        $form = $this->createBankForm($entity, $id);
        $form->handleRequest($request);

        $messageBroker = $this->get('app.message.broker');
        $messageDefaults = $this->container->getParameter('messages');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $form = $this->createBankForm($entity, $entity->getId());

            $messageBroker->addSuccess($messageDefaults['success']);

            return $this->redirect($this->generateUrl('bank_edit', ['id' => $entity->getId()]));
        } else {
            $messageBroker->addError($messageDefaults['error']);
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to save a entity.
     *
     * @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createBankForm(Bank $entity, $id = null)
    {
        $form = $this->createForm('AppBundle\Form\BankType', $entity, array(
            'action' => $this->generateUrl('bank_save', array('id' => $id)),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Creates a form to delete a entity.
     *
     *  @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm(Bank $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bank_delete', array('id' => $entity->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a confirmation message to delete an existing entity.
     *
     * @Route("/{id}/delete", name="bank_delete_confirm")
     * @Method({"GET"})
     */
    public function deleteConfirmAction(Request $request, Bank $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('AppBundle:Bank:delete.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Deletes a entity.
     *
     * @Route("/{id}", name="bank_delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Bank')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bank entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirectToRoute('bank_index');
    }
}
