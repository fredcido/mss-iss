<?php

namespace AppBundle\Controller;

use AppBundle\Entity\InterventionArea;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * InterventionArea controller.
 *
 * @Route("/intervention-area")
 * @Security("has_role('ROLE_ADMIN')")
 */
class InterventionAreaController extends Controller
{
    /**
     * Lists all InterventionArea entities.
     *
     * @Route("/", name="intervention_area_index", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:InterventionArea:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:InterventionArea')->findAll();

        return array(
            'entities' => $entities,
            'status' => \AppBundle\Enum\Status::get(),
        );
    }

    /**
     * @Route("/status")
     * @Method("POST")
     */
    public function statusAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $collection = $em->getRepository('AppBundle:InterventionArea')->findBy([
                'id' => $request->get('identifiers', null),
            ]);

            foreach ($collection as $entity) {
                $entity->setStatus($request->get('status'));
                $em->persist($entity);
            }

            $em->flush();

            return new JsonResponse(true);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    /**
     * Displays a form to create a new InterventionArea entity.
     *
     * @Route("/new", name="intervention_area_new")
     * @Method("GET")
     * @Template("AppBundle:InterventionArea:form.html.twig")
     */
    public function newAction()
    {
        $entity = new InterventionArea();
        $form = $this->createPostForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing InterventionArea entity.
     *
     * @Route("/{id}/edit", name="intervention_area_edit")
     * @Method("GET")
     * @Template("AppBundle:InterventionArea:form.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:InterventionArea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find InterventionArea entity.');
        }

        $editForm = $this->createPostForm($entity, $id);

        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
        );
    }

    /**
     * Creates a new InterventionArea entity.
     *
     * @Route("/save/{id}", name="intervention_area_save", defaults={"id": null})
     * @Method("POST")
     * @Template("AppBundle:InterventionArea:form.html.twig")
     */
    public function saveAction(Request $request, $id)
    {
        if (!$id) {
            $entity = new InterventionArea();
        } else {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:InterventionArea')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find InterventionArea entity.');
            }
        }

        $form = $this->createPostForm($entity, $id);
        $form->handleRequest($request);

        $messageBroker = $this->get('app.message.broker');
        $messageDefaults = $this->container->getParameter('messages');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $form = $this->createPostForm($entity, $entity->getId());

            $messageBroker->addSuccess($messageDefaults['success']);

            return $this->redirect($this->generateUrl('intervention_area_edit', ['id' => $entity->getId()]));
        } else {
            $messageBroker->addError($messageDefaults['error']);
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to save a InterventionArea entity.
     *
     * @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createPostForm(InterventionArea $entity, $id = null)
    {
        $form = $this->createForm('AppBundle\Form\InterventionAreaType', $entity, array(
            'action' => $this->generateUrl('intervention_area_save', array('id' => $id)),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Creates a form to delete a entity.
     *
     *  @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm(InterventionArea $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('intervention_area_delete', array('id' => $entity->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a confirmation message to delete an existing entity.
     *
     * @Route("/{id}/delete", name="intervention_area_delete_confirm")
     * @Method({"GET"})
     */
    public function deleteConfirmAction(Request $request, InterventionArea $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('AppBundle:InterventionArea:delete.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a entity.
     *
     * @Route("/{id}", name="intervention_area_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:InterventionArea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find InterventionArea entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirectToRoute('intervention_area_index');
    }
}
