<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Expense;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Enum\Status;

/**
 * controller.
 *
 * @Route("/expense")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ExpenseController extends Controller
{
    /**
     * Lists all entities.
     *
     * @Route("/", name="expense_index", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:Expense:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        return array(
            'referencies' => $em->getRepository('AppBundle:ExpenseReference')->findAll(),
            'status' => Status::get(),
        );
    }

    /**
     * @Route("/status")
     * @Method("POST")
     */
    public function statusAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $collection = $em->getRepository('AppBundle:Expense')->findBy([
                'id' => $request->get('identifiers', null),
            ]);

            foreach ($collection as $entity) {
                $entity->setStatus($request->get('status'));
                $em->persist($entity);
            }

            $em->flush();

            return new JsonResponse(true);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    /**
     * Displays a form to create a new entity.
     *
     * @Route("/new", name="expense_new")
     * @Method("GET")
     * @Template("AppBundle:Expense:form.html.twig")
     */
    public function newAction()
    {
        $entity = new Expense();
        $form = $this->createPostForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing entity.
     *
     * @Route("/{id}/edit", name="expense_edit", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:Expense:form.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Expense')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expense entity.');
        }

        $editForm = $this->createPostForm($entity, $id);

        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
        );
    }

    /**
     * Creates a new entity.
     *
     * @Route("/save/{id}", name="expense_save", defaults={"id": null})
     * @Method("POST")
     * @Template("AppBundle:Expense:form.html.twig")
     */
    public function saveAction(Request $request, $id)
    {
        if (!$id) {
            $entity = new Expense();
        } else {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Expense')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Expense entity.');
            }
        }

        $form = $this->createPostForm($entity, $id);
        $form->handleRequest($request);

        $messageBroker = $this->get('app.message.broker');
        $messageDefaults = $this->container->getParameter('messages');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $form = $this->createPostForm($entity, $entity->getId());

            $messageBroker->addSuccess($messageDefaults['success']);

            return $this->redirect($this->generateUrl('expense_index'));
        } else {
            $messageBroker->addError($messageDefaults['error']);
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to save a entity.
     *
     * @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createPostForm(Expense $entity, $id = null)
    {
        $form = $this->createForm('AppBundle\Form\ExpenseType', $entity, array(
            'action' => $this->generateUrl('expense_save', array('id' => $id)),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Creates a form to delete a entity.
     *
     *  @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm(Expense $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expense_delete', array('id' => $entity->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a confirmation message to delete an existing entity.
     *
     * @Route("/{id}/delete", name="expense_delete_confirm")
     * @Method({"GET"})
     */
    public function deleteConfirmAction(Request $request, Expense $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('AppBundle:Expense:delete.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a entity.
     *
     * @Route("/{id}/delete", name="expense_delete")
     * @Method("GET")
     * 
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Expense')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expense entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirectToRoute('expense_index');
    }
    
    /**
     * 
     * @Route("/{id}/expense-type", name="expense_expense_type", options={"expose"=true})
     * @Method("GET")
     */
    public function expenseTypeAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $collection = $em->getRepository('AppBundle:ExpenseType')->findByExpenseReference($id);
        
        $data = array();
        
        foreach ($collection as $entity) {
            $data[] = [
                'value' => $entity->getId(),
                'text'  => $entity->getName(),
            ];
        }
        
        return new JsonResponse($data);
    }

    /**
     * 
     * @Route("/expense-list", name="expense_list", options={"expose"=true})
     * @Method("GET")
     */
    public function expenseListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $findBy = [];
        
        if ($request->get('expense_reference_id', null)) {
            $findBy['expenseReference'] = $request->get('expense_reference_id');
        }
        
        if ($request->get('show_only_actives', null)) {
            $findBy['status'] = Status::ACTIVE;
        }
        
        $rootCollection = $em->getRepository('AppBundle:Expense')->getRootExpenses($findBy);
        
        $tree = array();
        foreach ($rootCollection as $entity) {
            $tree[] = [
                'id' => $entity->getId(),
                'code' => $entity->getCode(),
                'status' => $entity->getStatus(),
                'name'  => $entity->getName(),
                'children' => $this->getChildren($entity, $findBy)
            ];
        }
        return new JsonResponse($tree);
    }
    
    public function getChildren(Expense $expense, $findBy) {
        $em = $this->getDoctrine()->getManager();
        $expenseRepository = $em->getRepository('AppBundle:Expense');

        $itens = [];
        
        if (!$expense->getChildren()->count()) {
            return $itens;
        }
        
        foreach($expenseRepository->getChildrenOf($expense->getId(), $findBy) as $item) {
            $element = [
                'id' => $item->getId(),
                'code' => $item->getCode(),
                'status' => $item->getStatus(),
                'name'  => $item->getName(),
                'children' => $this->getChildren($item, $findBy)
            ];
            
            $itens[] = $element;
        }
        
        return $itens;
    }
    
    
    /**
     * 
     * @Route("/expense-change-parent", name="expense_change_parent", options={"expose"=true})
     * @Method("POST")
     */
    public function expenseChangeParent(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $expense = $em->getRepository('AppBundle:Expense')->find($request->get('expenseId'));
            
            if (null === $expense) {
                throw new Exception(sprintf("Expense with id = '%s' does not exist", $request->get('expenseId')));
            }
            
            $newParent = null;
            if ($request->get('newParentId')) {
                $newParent = $em->getRepository('AppBundle:Expense')->find($request->get('newParentId'));
                
                if (null === $newParent) {
                    throw new Exception(sprintf("Expense with id = '%s' does not exist", $request->get('expenseId')));
                }
            }
            
            // update parent
            $expense->setParent($newParent);
            $em->persist($expense);
            $em->flush();

            return new JsonResponse(true);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
        
    }
}
