<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Setting;

/**
 * Settings controller
 *
 * @Route("/settings")
 * @Security("has_role('ROLE_ADMIN')")
 */
class SettingController extends Controller {

    /**
     * Display the setting list
     *
     * @Route("/", name="setting_index", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:Setting:index.html.twig")
     */
    public function indexAction() {
        $csrf = $this->get('security.csrf.token_manager');
        $token = $csrf->refreshToken('adriano');

        return [ 'token' => $token ];
    }

    /**
     * Save a value for a setting
     *
     * @Route("/save/{id}", name="setting_save", options={"expose"=true})
     * @Method("POST")
     */
    public function saveAction(Request $request, $id) {
        $messageDefaults = $this->container->getParameter('messages');
        $messages = $this->get('app.messages');
        $em = $this->getDoctrine()->getManager();

        $result = [];

        try {
            $entity = $em->getRepository('AppBundle:Setting')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Setting entity.');
            }

            // set value on entity and save
            $entity->setValue($request->request->get('value'));
            
            $em->persist($entity);
            $em->flush();

            $result['text'] = $messages->get($messageDefaults['success']);
            $result['type'] = 'success';

            return new JsonResponse($result);

            /*$form = $this->createPostForm($entity, $id);

            $form->get('value')->submit($request->request->get('value'));

            if ($form->isValid()) {
                $em->persist($entity);
                $em->flush();

                $result['text'] = $messages->get($messageDefaults['success']);
                $result['type'] = 'success';
            } else {
                $formJsonErrors = $this->get('app.forms.errors_json');

                $result['text']   = 'Validation Error'; //$messages->get($messageDefaults['validation_error']);
                $result['type']   = 'validation_error';
                $result['errors'] = $formJsonErrors->getErrors($form);
            }*/

        } catch (\Exception $e) {
            $result['text'] =  $e->getMessage();
            $result['type'] = 'error';
        }

        return new JsonResponse($result);
    }

    /**
     * Creates a form to save a entity.
     *
     * @param Setting $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createPostForm(Setting $entity, $id)
    {
        $form = $this->createForm('AppBundle\Form\SettingType', $entity, array(
            'action' => $this->generateUrl('setting_save', array('id' => $id)),
            'method' => 'POST',
        ));

        return $form;
    }
    

}