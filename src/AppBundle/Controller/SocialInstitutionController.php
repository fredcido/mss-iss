<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SocialInstitution;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Enum\Status;

/**
 * controller.
 *
 * @Route("/social-institution")
 * @Security("has_role('ROLE_ADMIN')")
 */
class SocialInstitutionController extends Controller
{
    /**
     * Lists all entities.
     *
     * @Route("/", name="social_institution_index", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:SocialInstitution:index.html.twig")
     */
    public function indexAction()
    {
        $em        = $this->getDoctrine()->getManager();
        $areas     = $em->getRepository('AppBundle:InterventionArea')->findBy(['status' => Status::ACTIVE]);
        $districts = $em->getRepository('AppBundle:Municipio')->findAll();
        $responses = $em->getRepository('AppBundle:SocialResponse')->findBy(['status' => Status::ACTIVE]);

        return array(
            'status'             => \AppBundle\Enum\Status::get(),
            'districts'          => $districts,
            'intervention_areas' => $areas,
            'social_responses'   => $responses,
        );
    }

    /**
     * Lists all entities.
     *
     * @Route("/list", name="social_institution_list", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:SocialInstitution:list.html.twig")
     */
    public function listAction(Request $request)
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:SocialInstitution')->search($request->query->all());

        return array(
            'entities' => $entities,
            'status'   => \AppBundle\Enum\Status::get(),
        );
    }

    /**
     * Lists all entities.
     *
     * @Route("/dialog-intervention-area/{intervention_area_id}", name="social_institution_dialog_intervention_area", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:SocialInstitution:dialogInterventionArea.html.twig")
     */
    public function dialogInterventionAreaAction($intervention_area_id)
    {
        return array(
            'intervention_area_id' => $intervention_area_id,
        );
    }

    /**
     * @Route("/status")
     * @Method("POST")
     */
    public function statusAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $collection = $em->getRepository('AppBundle:SocialInstitution')->findBy([
                'id' => $request->get('identifiers', null),
            ]);

            foreach ($collection as $entity) {
                $entity->setStatus($request->get('status'));
                $em->persist($entity);
            }

            $em->flush();

            return new JsonResponse(true);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

    /**
     * Displays a form to create a new entity.
     *
     * @Route("/new", name="social_institution_new")
     * @Route("/{id}/edit", name="social_institution_edit")
     *
     * @Method("GET")
     * @Template("AppBundle:SocialInstitution:form.html.twig")
     */
    public function formAction($id = null)
    {
        $entity = new SocialInstitution();
        $form   = $this->createPostForm($entity);

        return array(
            'entity' => $entity,
            'id'     => $id,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/{id}/get", name="social_institution_retrieve", options={"expose"=true})
     *
     * @Method("GET")
     */
    public function getAction($id)
    {
        $em          = $this->getDoctrine()->getManager();
        $entity      = $em->getRepository('AppBundle:SocialInstitution')->find($id);
        $serializer  = $this->get('jms_serializer');
        $jsonContent = $serializer->serialize($entity, 'json');

        return new Response($jsonContent);
    }

    /**
     * Creates a new entity.
     *
     * @Route("/save/{id}", name="social_institution_save", defaults={"id": null}, options={"expose"=true})
     * @Method("POST")
     */
    public function saveAction(Request $request, $id)
    {
        $messageDefaults = $this->container->getParameter('messages');
        $messages        = $this->get('app.messages');
        $translator      = $this->get('translator');

        try {
            if (!$id) {
                $entity = new SocialInstitution();
            } else {
                $em     = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('AppBundle:SocialInstitution')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find Social Institution entity.');
                }
            }

            $form = $this->createPostForm($entity, $id);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $message['text'] = $messages->get($messageDefaults['success']);
                $message['type'] = 'success';
            } else {
                $formJsonErrors = $this->get('app.forms.errors_json');

                $message['text']   = $messages->get($messageDefaults['validation_error']);
                $message['type']   = 'validation_error';
                $message['errors'] = $formJsonErrors->getErrors($form);
            }
        } catch (\Exception $e) {
            $message['text'] = $e->getMessage(); //$messages->get($messageDefaults['error']);
            $message['type'] = 'error';
        }

        return new JsonResponse($message);
    }

    /**
     * Creates a form to save a entity.
     *
     * @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createPostForm(SocialInstitution $entity, $id = null)
    {
        $form = $this->createForm('AppBundle\Form\SocialInstitutionType', $entity, array(
            'action' => $this->generateUrl('social_institution_save', array('id' => $id)),
            'method' => 'POST',
        ));

        return $form;
    }
}
