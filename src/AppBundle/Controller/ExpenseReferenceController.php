<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ExpenseReference;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @Route("/expense-reference")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ExpenseReferenceController extends Controller
{
    /**
     * Lists all entities.
     *
     * @Route("/", name="expense_reference_index", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:ExpenseReference:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:ExpenseReference')->findAll();

        return [
            'entities' => $entities,
        ];
    }

    /**
     * Displays a form to create a new entity.
     *
     * @Route("/new", name="expense_reference_new")
     * @Method("GET")
     * @Template("AppBundle:ExpenseReference:form.html.twig")
     */
    public function newAction()
    {
        $entity = new ExpenseReference();
        $form = $this->createPostForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing entity.
     *
     * @Route("/{id}/edit", name="expense_reference_edit")
     * @Method("GET")
     * @Template("AppBundle:ExpenseReference:form.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:ExpenseReference')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExpenseReference entity.');
        }

        $editForm = $this->createPostForm($entity, $id);

        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
        );
    }

    /**
     * Creates a new entity.
     *
     * @Route("/save/{id}", name="expense_reference_save", defaults={"id": null})
     * @Method("POST")
     * @Template("AppBundle:ExpenseReference:form.html.twig")
     */
    public function saveAction(Request $request, $id)
    {
        if (!$id) {
            $entity = new ExpenseReference();
        } else {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:ExpenseReference')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ExpenseReference entity.');
            }
        }

        $form = $this->createPostForm($entity, $id);
        $form->handleRequest($request);

        $messageBroker = $this->get('app.message.broker');
        $messageDefaults = $this->container->getParameter('messages');

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $form = $this->createPostForm($entity, $entity->getId());

            $messageBroker->addSuccess($messageDefaults['success']);

            return $this->redirect($this->generateUrl('expense_reference_edit', ['id' => $entity->getId()]));
        } else {
            $messageBroker->addError($messageDefaults['error']);
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to save a entity.
     *
     * @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createPostForm(ExpenseReference $entity, $id = null)
    {
        $form = $this->createForm('AppBundle\Form\ExpenseReferenceType', $entity, array(
            'action' => $this->generateUrl('expense_reference_save', array('id' => $id)),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Creates a form to delete a entity.
     *
     *  @param InterventionArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createMainForm(ExpenseReference $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expense_reference_main', array('id' => $entity->getId())))
            ->setMethod('POST')
            ->getForm();
    }

    /**
     *
     * @Route("/{id}/main", name="expense_reference_main_confirm")
     * @Method({"GET"})
     */
    public function mainConfirmAction(Request $request, ExpenseReference $entity)
    {
        $form = $this->createMainForm($entity);

        return $this->render('AppBundle:ExpenseReference:main.html.twig', [
            'entity' => $entity,
            'main_form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a entity.
     *
     * @Route("/{id}", name="expense_reference_main")
     */
    public function mainAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:ExpenseReference');
        
        $collection = $repository->findByFlag(1);
        
        foreach ($collection as $entity) {
            $entity->setFlag(0);
            $em->persist($entity);
        }
        
        $entity = $repository->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExpenseReference entity.');
        }
        
        $entity->setFlag(1);
        $em->persist($entity);
        
        $em->flush();

        return $this->redirectToRoute('expense_reference_index');
    }
}
