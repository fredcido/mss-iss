<?php

namespace AppBundle\Controller;

use AppBundle\Service;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * controller.
 *
 * @Route("/relationship-expense")
 * @Security("has_role('ROLE_ADMIN')")
 */
class RelationshipExpenseController extends Controller
{
    /**
     * Lists all entities.
     *
     * @Route("/", name="relationship_expense_index", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:RelationshipExpense:index.html.twig")
     */
    public function indexAction()
    {
        /* @var $service \AppBundle\Service\RelationshipExpense */
        $service = $this->get('app.relationship_expense');
        $source = $service->getExpenseReferenceSource();
        
        return [
            'source'   => $source,
            'targets'  => $service->getExpenseReferenceTargetCollection(),
        ];
    }
    
    /**
     * @Route("/{id}/relationship", name="relationship_expense_relationship", options={"expose"=true})
     * @Method("GET")
     * @Template("AppBundle:RelationshipExpense:relationship.html.twig")
     */
    public function relationshipAction($id)
    {
        /* @var $service \AppBundle\Service\RelationshipExpense */
        $service = $this->get('app.relationship_expense');
        
        $source = $service->getExpenseReferenceSource();
        $target = $service->getExpenseReferenceTargetById($id);
        
        $expenses = $service->getExpenseByIdExpenseReferenceTarget($id);
        
        $selected = array();
        
        /* @var $expense \AppBundle\Entity\Expense */
        foreach ($expenses as $expense) {
            /* @var $target \AppBundle\Entity\Expense */
            foreach ($expense->getExpenseTarget() as $expenseTarget) {
                $selected[$expense->getId()][] = $expenseTarget->getId();
            }
        }
        
        return [
            'sources'  => $service->getExpenseArrayByExpenseReference($source),
            'targets'  => $service->getExpenseArrayByExpenseReference($target),
            'expenses' => $expenses,
            'selected' => $selected,
        ];
    }
    
    /**
     *
     * @Route("/{id}/expense", name="relationship_expense_expense", options={"expose"=true})
     * @Method("GET")
     */
    public function expenseAction(Request $request, $id)
    {
        /* @var $service \AppBundle\Service\RelationshipExpense */
        $service = $this->get('app.relationship_expense');
        
        $source = $service->getExpenseReferenceSource();
        $target = $service->getExpenseReferenceTargetById($id);
        
        return new JsonResponse([
            'source' => $service->getExpenseArrayByExpenseReference($source),
            'target' => $service->getExpenseArrayByExpenseReference($target)
        ]);
    }
    
    /**
     * Creates a new entity.
     *
     * @Route("/save/", name="relationship_expense_save", options={"expose"=true})
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {
        try {
            /* @var $service \AppBundle\Service\RelationshipExpense */
            $service = $this->get('app.relationship_expense');
            $service->save($request->request->all());
            
            return new JsonResponse(true);
        } catch (\Exception $e) {
            return new JsonResponse(false);
        }
    }
}