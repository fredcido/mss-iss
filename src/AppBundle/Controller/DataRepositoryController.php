<?php

namespace AppBundle\Controller;

use AppBundle\Enum\PlaceOwnership;
use AppBundle\Enum\SupportMss;
use AppBundle\Enum\Status;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/data", condition="request.isXmlHttpRequest()")
 * @Security("has_role('ROLE_USER')")
 */
class DataRepositoryController extends Controller
{
    /**
     * @Route("/posts", name="data_posts", options={"expose"=true})
     * @Method("GET")
     */
    public function getPostsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Post')->findBy(['status' => Status::ACTIVE]);
        $serializer = $this->get('serializer');
        return new JsonResponse($serializer->serialize($entity, 'json'), 200, array(), true);
    }

    /**
     * @Route("/settings", name="data_settings", options={"expose"=true})
     * @Method("GET")
     */
    public function getSettingsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Setting')->findAll();
        $serializer = $this->get('serializer');
        return new JsonResponse($serializer->serialize($entity, 'json'), 200, array(), true);
    }

    /**
     * @Route("/description-types", name="data_description_types", options={"expose"=true})
     * @Method("GET")
     */
    public function getDescriptionTypesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:DescriptionType')->findAll();
        $serializer = $this->get('serializer');
        return new JsonResponse($serializer->serialize($entity, 'json'), 200, array(), true);
    }

    /**
     * @Route("/intervention-areas", name="data_intervention_areas", options={"expose"=true})
     * @Method("GET")
     */
    public function getInterventionAreasAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:InterventionArea')->findBy(['status' => Status::ACTIVE]);
        $serializer = $this->get('serializer');
        return new JsonResponse($serializer->serialize($entity, 'json'), 200, array(), true);
    }

    /**
     * @Route("/social-responses", name="data_social_responsoes", options={"expose"=true})
     * @Route("/social-responses/intervention-area/{id}", name="data_social_responses_by_intervention_area", options={"expose"=true})
     * @Method("GET")
     */
    public function getSocialResponsesAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();

        if (!is_null($id)) {
            $interventionArea = $em->getRepository('AppBundle:InterventionArea')->find($id);
            $entity = $interventionArea->getSocialResponses();
        } else {
            $repository = $em->getRepository('AppBundle:SocialResponse');
            $entity = $repository->findBy(['status' => Status::ACTIVE]);
        }
        $serializer = $this->get('serializer');
        return new JsonResponse($serializer->serialize($entity, 'json'), 200, array(), true);
    }

    /**
     * @Route("/municipios", name="data_municipios", options={"expose"=true})
     * @Method("GET")
     */
    public function getMunicipiosAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Municipio')->findAll();
        $serializer = $this->get('serializer');
        return new JsonResponse($serializer->serialize($entity, 'json'), 200, array(), true);
    }

    /**
     * @Route("/postos-administrativos", name="data_postos_admnistrativos", options={"expose"=true})
     * @Route("/postos-administrativos/municipio/{id}", name="data_postos_administrativos_by_municipio")
     * @Method("GET")
     */
    public function getPostoAdministrativosAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:PostoAdministrativo');

        if (is_null($id)) {
            $entity = $repository->findAll();
        } else {
            $entity = $repository->findBy(['municipio' => $id]);
        }

        $serializer = $this->get('serializer');
        return new JsonResponse($serializer->serialize($entity, 'json'), 200, array(), true);
    }

    /**
     * @Route("/sukus", name="data_sukus", options={"expose"=true})
     * @Route("/sukus/posto-administrativo/{id}", name="data_suku_by_posto_administrativo")
     * @Method("GET")
     */
    public function getSukusAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Suku');

        if (is_null($id)) {
            $entity = $repository->findAll();
        } else {
            $entity = $repository->findBy(['postoAdministrativo' => $id]);
        }

        $serializer = $this->get('serializer');
        return new JsonResponse($serializer->serialize($entity, 'json'), 200, array(), true);
    }

    /**
     * @Route("/place-ownership", name="data_place_ownership", options={"expose"=true})
     * @Method("GET")
     */
    public function getPlaceOwnershipAction()
    {
        $data = PlaceOwnership::getOptions();
        $rows = array();
        foreach ($data as $id => $label) {
            $rows[] = ['id' => $id, 'name' => $label];
        }

        return new JsonResponse(json_encode($rows), 200, array(), true);
    }

    /**
     * @Route("/support-mss", name="data_support_mss", options={"expose"=true})
     * @Method("GET")
     */
    public function getSupportMssAction()
    {
        $data = SupportMss::getOptions();
        $rows = array();
        foreach ($data as $id => $label) {
            $rows[] = ['id' => $id, 'name' => $label];
        }

        return new JsonResponse(json_encode($rows), 200, array(), true);
    }

    /**
     * @Route("/status", name="data_status", options={"expose"=true})
     * @Method("GET")
     */
    public function getStatusAction()
    {
        $data = Status::getOptions();
        $rows = array();
        foreach ($data as $id => $label) {
            $rows[] = ['id' => $id, 'name' => $label];
        }

        return new JsonResponse(json_encode($rows), 200, array(), true);
    }

    /**
     * @Route("/banks", name="data_banks", options={"expose"=true})
     * @Method("GET")
     */
    public function getBanksAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Bank')->findBy(['status' => Status::ACTIVE]);
        $serializer = $this->get('serializer');
        return new JsonResponse($serializer->serialize($entity, 'json'), 200, array(), true);
    }

    /**
     * @Route("/institution-status", name="data_institution_status", options={"expose"=true})
     * @Method("GET")
     */
    public function getInstitutionStatusAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:InstitutionStatus')->findAll();
        $serializer = $this->get('serializer');
        return new JsonResponse($serializer->serialize($entity, 'json'), 200, array(), true);
    }
}
