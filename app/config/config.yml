imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }
    - { resource: "@MenuBundle/Resources/config/services.yml" }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: pt

framework:
    #esi:             ~
    translator:      { fallbacks: ["%locale%"] }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    serializer:
        enabled: true
    templating:
        engines: ['twig']
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # http://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id:  session.handler.native_file
        save_path:   "%kernel.root_dir%/../var/sessions/%kernel.environment%"
    fragments:       ~
    http_method_override: true
    assets: ~

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_mysql
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: "%kernel.root_dir%/data/data.db3"
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #     path:     "%database_path%"

    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }

# Configs for user bundle (FOSUserBundle)
fos_user:
    db_driver: orm # other valid values are 'mongodb', 'couchdb' and 'propel'
    firewall_name: main
    user_class: AppBundle\Entity\User
    registration:
        form:
            type: AppBundle\Form\UserType

# Configs for translation bundle (JMSTranslationBundle)
jms_translation:
    configs:
        app:
            dirs: [%kernel.root_dir%, %kernel.root_dir%/../src]
            output_dir: %kernel.root_dir%/../src/AppBundle/Resources/translations
            output_format: xliff
            ignored_domains: [routes]
            excluded_names: ["*TestCase.php", "*Test.php"]
            excluded_dirs: [cache, data, logs, FOSUserBundle]
            extractors: []

# Configs for pagination bundle (KnpPaginatorBundle)
knp_paginator:
    page_range: 5                      # default page range used in pagination control
    default_options:
        page_name: page                # page query parameter name
        sort_field_name: sort          # sort field query parameter name
        sort_direction_name: direction # sort direction query parameter name
        distinct: true                 # ensure distinct results, useful when ORM queries are using GROUP BY statements
    template:
        pagination: KnpPaginatorBundle:Pagination:twitter_bootstrap_v3_pagination.html.twig     # sliding pagination controls template
        sortable: KnpPaginatorBundle:Pagination:sortable_link.html.twig # sort link template

# Configs for LuneticsLocaleBundle
lunetics_locale:
    guessing_excluded_pattern: ^/login

    session:
        variable: "_locale"

    guessing_order:
        - query
        - router
        - session
        - cookie
        - browser

    allowed_locales:
        - en
        - pt
        - tl


    switcher:
        template: 'includes/language-bar.html.twig'
        show_current_locale: true
        use_controller: true                # Must be set to true if you want to use the controller. Defaults to false
        use_referrer: true                  # Redirect to the origin url from where the switcher was used. Defaults to true
        redirect_to_route: homepage         # This parameter MUST be set. Fallback route if no referrer could be found.
        redirect_statuscode: 302            # Redirect HTTP status code. Options:  300, 301, 302, 303, 307. Defaults to 302


bazinga_js_translation:
    default_domain:       app

# AsseticBundle
assetic:
    debug:          '%kernel.debug%'
    use_controller: '%kernel.debug%'
    filters:
        cssrewrite: ~

parameters:
    jms_serializer.doctrine_proxy_subscriber.class: AppBundle\Serializer\DoctrineProxySubscriber