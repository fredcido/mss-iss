(function (Translator) {
    // tl
    Translator.add("fos_user.email.already_used", "fos_user.email.already_used", "validators", "tl");
    Translator.add("fos_user.email.blank", "fos_user.email.blank", "validators", "tl");
    Translator.add("fos_user.email.invalid", "fos_user.email.invalid", "validators", "tl");
    Translator.add("fos_user.email.long", "fos_user.email.long", "validators", "tl");
    Translator.add("fos_user.email.short", "fos_user.email.short", "validators", "tl");
    Translator.add("fos_user.password.blank", "fos_user.password.blank", "validators", "tl");
    Translator.add("fos_user.password.short", "fos_user.password.short", "validators", "tl");
    Translator.add("fos_user.username.already_used", "fos_user.username.already_used", "validators", "tl");
    Translator.add("fos_user.username.blank", "fos_user.username.blank", "validators", "tl");
    Translator.add("fos_user.username.long", "fos_user.username.long", "validators", "tl");
    Translator.add("fos_user.username.short", "fos_user.username.short", "validators", "tl");
})(Translator);
