(function (Translator) {
    // en
    Translator.add("BeneficiaryClass", "BeneficiaryClass", "messages", "en");
    Translator.add("SocialResponse", "SocialResponse", "messages", "en");
    Translator.add("crud.action.confirm_inactive", "crud.action.confirm_inactive", "messages", "en");
    Translator.add("crud.action.create", "crud.action.create", "messages", "en");
    Translator.add("crud.action.delete", "crud.action.delete", "messages", "en");
    Translator.add("crud.action.edit", "crud.action.edit", "messages", "en");
    Translator.add("crud.action.inactive", "crud.action.inactive", "messages", "en");
    Translator.add("crud.action.new", "crud.action.new", "messages", "en");
    Translator.add("crud.action.save", "crud.action.save", "messages", "en");
    Translator.add("crud.back-to-index-link", "crud.back-to-index-link", "messages", "en");
    Translator.add("edit_BeneficiaryClass", "edit_BeneficiaryClass", "messages", "en");
    Translator.add("edit_InterventionArea", "edit_InterventionArea", "messages", "en");
    Translator.add("edit_SocialResponse", "edit_SocialResponse", "messages", "en");
    Translator.add("edit_user", "edit_user", "messages", "en");
    Translator.add("home", "home", "messages", "en");
    Translator.add("inactive_BeneficiaryClass", "inactive_BeneficiaryClass", "messages", "en");
    Translator.add("inactive_InterventionArea", "inactive_InterventionArea", "messages", "en");
    Translator.add("inactive_SocialResponse", "inactive_SocialResponse", "messages", "en");
    Translator.add("new_BeneficiaryClass", "new_BeneficiaryClass", "messages", "en");
    Translator.add("new_InterventionArea", "new_InterventionArea", "messages", "en");
    Translator.add("new_SocialResponse", "new_SocialResponse", "messages", "en");
    Translator.add("new_user", "new_user", "messages", "en");
    Translator.add("settings", "settings", "messages", "en");
    Translator.add("users", "users", "messages", "en");
})(Translator);
