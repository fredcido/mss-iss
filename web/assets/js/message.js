Message = {
    
    fadeOut: true,
    
    setFadeOut: function( flag )
    {
		this.fadeOut = flag;
		return this;
    },
    
    clearMessages: function( container )
    {
		var container = this.getContainer(container);
		
		var divContainer = $( container );

		if ( divContainer.find( '.alert' ).length )
		    divContainer.find( '.alert:not(.not-remove)' ).remove();
		
		return this;
    },
    
    showBulkMessages: function( messages, container )
    {
		var container = this.getContainer(container);
		
		var divContainer = $( container );
		if ( divContainer.find( '.alert' ).length )
		    divContainer.find( '.alert:not(.not-remove)' ).remove();
		
		var messagesDivs = [];
		for ( i in messages ) {
		    
		    var msgDiv = $( '<div />' );
		    var level = 'danger'
		    msgDiv.addClass( 'alert alert-' + level );

		    var buttonClose = $( '<button />' );
		    buttonClose.attr( 'type', 'button' )
				.addClass( 'close' )
				.attr( 'data-dismiss', 'alert' );

		    msgDiv.html( Translator.trans(messages[i], {}, 'app') );
		    msgDiv.prepend( buttonClose );

		    divContainer.prepend( msgDiv );

		    General.scrollTo( msgDiv );

		    msgDiv.fadeIn();
		    messagesDivs.push( msgDiv );
		}
		
		if ( this.fadeOut ) {

			setTimeout(
			    function()
			    {
				for ( i in messagesDivs ) {
				    
				    var msgDiv = messagesDivs[i];
				    msgDiv.fadeOut( 'slow',
					function()
					{
					    msgDiv.remove();
					}
				    );
				}
			    },
			    10 * 1000
			);
		    }
		
		this.fadeOut = true;
		
		return this;
    },
    
    getContainer: function( container )
    {
		var container = $(container);
		if ( container.length )
		    return container;
		
		var rowMessage = $('<div>').addClass('row');
		var colMessage = $('<div>').addClass('col-xs-12');
		var container = $('<div>');
		
		rowMessage.append(colMessage.append(container));
		$('.container').prepend(container);
		
		return container;
    },

    showMsg: function( text, type, container, scroll )
    {
		var container = this.getContainer(container);
		
		var divContainer = $( container );
		if ( divContainer.find( '.alert' ).length )
		    divContainer.find( '.alert:not(.not-remove)' ).remove();

		var msgDiv = $( '<div />' );
		msgDiv.addClass( 'alert bold alert-' + type );

		var buttonClose = $( '<button />' );
		buttonClose.attr( 'type', 'button' )
			    .addClass( 'close' )
			    .attr( 'data-dismiss', 'alert' );

		msgDiv.html( Translator.trans(text, {}, 'app') );
		msgDiv.prepend( buttonClose );

		divContainer.prepend( msgDiv );

		if ( scroll === undefined || scroll === true )
			General.scrollTo( msgDiv );

		msgDiv.fadeIn();

		if ( this.fadeOut ) {
		    
		    setTimeout(
			function()
			{
			    msgDiv.fadeOut( 'slow',
				function()
				{
				    msgDiv.remove();
				}
			    );
			},
			10 * 1000
		    );
		}
		
		this.fadeOut = true;
		
		return this;	
    },

    msgError: function( text, container, scroll )
    {
		this.showMsg( text, 'danger', container, scroll );
		
		return this;
    },

    msgInfo: function( text, container, scroll )
    {
		this.showMsg( text, 'info', container, scroll );
		
		return this;
    },

    msgSuccess: function( text, container, scroll )
    {
		this.showMsg( text, 'success', container, scroll );
		
		return this;
    }
}