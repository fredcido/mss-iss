(function ($, undefined) {
	"use strict";
        
        if($.jstree.plugins.editicon) { return; }

        var _i = document.createElement('i');
	_i.className = 'fa fa-pencil';
        _i.setAttribute('role', 'presentation');

        var _a = document.createElement('a');
        _a.href = "http://www.google.com.br";
        _a.appendChild(_i);

	// extending the defaults
	$.jstree.defaults.editicon = {
            
	};

	// the actual plugin code
	$.jstree.plugins.editicon = function (options, parent) {

		// *SPECIAL* FUNCTIONS
		this.init = function (el, options) {
			// do not forget parent
			parent.init.call(this, el, options);
		};
                
                this.redraw_node = function(obj, deep, is_callback, force_render) {
                    var self = this;
                    
                    obj = parent.redraw_node.apply(this, arguments);
                    
                    if(obj) {
                            var i, j, tmp = null, icon = null;
                            for(i = 0, j = obj.childNodes.length; i < j; i++) {
                                    if(obj.childNodes[i] && obj.childNodes[i].className && obj.childNodes[i].className.indexOf("jstree-anchor") !== -1) {
                                            tmp = obj.childNodes[i];
                                            break;
                                    }
                            }
                            if(tmp) {
                                    icon = _a.cloneNode(true);
                                    icon.addEventListener('click', function(event) {
                                        event.preventDefault();
                                        event.stopPropagation();
                                        self.trigger('edit_node', { 'node' : self.get_node(obj) });
                                    });
                                    tmp.insertBefore(icon, tmp.childNodes[0]);
                            }
}                    
                    return obj;
                }
                
		
	};

	// attach to document ready if needed
	$(function () {
		// do(stuff);
	});

	// you can include the sample plugin in all instances by default
	$.jstree.defaults.plugins.push("editicon");
})(jQuery);
