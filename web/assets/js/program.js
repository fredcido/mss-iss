Program = {
    init: function() {
        this.configForm();
        //this.configAddExpense();
        this.configGrid();
    },

    configForm: function() {
        var form = $('.form form');
        var submit = function() {
            form.preventDefault();
            var obj = {};

            Form.submitAjax(form, obj);
            return false;
        };
        Form.addValidate(form, submit);
    },

    configAddExpense: function() {
        /*var self = this;
        var $collectionHolder;

        // setup an "add a tag" link
        var $addTagLink = $('<a href="#" class="add_expense_link">Add a Expense</a>');
        var $newLinkLi = $('<li></li>').append($addTagLink);

        jQuery(document).ready(function() {
            // Get the ul that holds the collection of tags
            $collectionHolder = $('#appbundle_program_expenses');

            // add the "add a tag" anchor and li to the tags ul
            $collectionHolder.append($newLinkLi);

            // count the current form inputs we have (e.g. 2), use that as the new
            // index when inserting a new item (e.g. 2)
            $collectionHolder.data('index', $collectionHolder.find(':input').length);

            $addTagLink.on('click', function(e) {
                // prevent the link from creating a "#" on the URL
                e.preventDefault();

                // add a new tag form (see next code block)
                self.addExpenseForm($collectionHolder, $newLinkLi);
            });
        });*/
    },

    addExpenseForm: function($collectionHolder, $newLinkLi) {
        var prototype = $collectionHolder.data('prototype');console.log(prototype);
        var index = $collectionHolder.data('index');
        var newForm = prototype.replace(/__name__/g, index);
        var $newFormLi = $('<li></li>').append(newForm);

        $collectionHolder.data('index', index + 1);
        $newLinkLi.before($newFormLi);
    },
    
    configGrid: function() {
    	$(".change-status").on("click", this.changeStatus);
    	$(".toggle-check").on("click", this.toggleCheck);
    },
    
    changeStatus: function() {
    	if (!$(".status-check:checked").length) {
    		Message.msgError('Select the row to change the status');
			return false;
    	}
    	
    	var data = {
    		status: $(this).data("status"),
    		identifiers: []
    	};
    	
    	$(".status-check:checked").each(function() {
    		data.identifiers.push($(this).val());
    	});
    	
    	$.post(location.href.replace(/\/$/, "/status"), data)
    		.done(function() {
    			Message.msgSuccess('Status changed.');
    			
    			var callback = function() {
    				document.location.reload(true);
    			};
    			
    			window.setTimeout(callback, 1000);
    		}).fail(function() {
    			Message.msgError('The status could not be changed.');
    		});
    },
    
    toggleCheck: function()
    {
    	$(".status-check").each(function() {
    		$(this).prop("checked", !$(this).is(":checked"));
    	});
    }
};

// Excecute
$(document).ready(function() {
    Program.init();
});
