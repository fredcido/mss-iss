var RelationshipExpense = {
	
	source: [],
	
	target: [],
		
	init: function() {
		this.config();
	},
	
	config: function() {
		$(".add_relationship").on("click", this.addRelationship);
		$(".save_relationship").on("click", this.saveRelationship);
		
		$("#expense_reference").on("change", this.changeExpenseReference);
		
		$("tbody#relationship").on("click", ".delete_relationship", this.deleteRelationship);
		$("tbody#relationship").on("change", ".expense_source", this.changeExpenseSource);
	},
	
	changeExpenseReference: function() {
		if (! $(this).val()) {
			RelationshipExpense.clear();
			return;
		}
		
		$(".add_relationship, .save_relationship").prop("disabled", false);
		
		RelationshipExpense.loadExpense();
	},
	
	changeExpenseSource: function() {
		$("tbody#relationship tr")
			.eq($(".expense_source").index(this))
			.find("select.expense_target")
			.attr("name", "expense_target[" + $(this).val() + "][]");
	},
	
	loadRelationship: function(id) {
		$.get(Routing.generate('relationship_expense_relationship', {id: id}))
			.done(function(html) {
				if (html) {
					$("tbody#relationship").empty().append(html);
					$(".expense_source, .expense_target").select2({
						width: "resolve",
						placeholder: "Choose one",
						allowClear: true,
						escapeMarkup: function (markup) { return markup; }
					});
				}
			})
			.fail(function() {
				Message.msgError('Error: fail to load content.');
			});
	},
	
	addRelationship: function() {
		var 
			source = $("<select>")
			target = $("<select>");
		
		$("tbody#relationship").prepend(
			$("<tr>").append(
				$("<td>").append(
					$(source)
						.addClass("form-control expense_source")
						.css("width", "390px")
						.attr("name", "expense_source[]")
				),
				$("<td>").append(
					$(target)
						.addClass("form-control expense_target")
						.css("width", "390px")
						.attr("multiple", "multiple")
				),
				$("<td>").append(
					$("<button>")
						.attr("type", "button")
						.addClass("btn red delete_relationship")
						.append($("<i>").addClass("fa fa-trash"))
				)
			)
		);
		
		$(source)
			.select2({
				width: "resolve",
				placeholder: "Choose one",
				allowClear: true,
				escapeMarkup: function (markup) { return markup; },
				data: RelationshipExpense.source
			});
		
		$(source).val("").trigger("change");
		
		$(target)
			.select2({
				width: "resolve",
				placeholder: "Choose one",
				allowClear: true,
				escapeMarkup: function (markup) { return markup; },
				data: RelationshipExpense.target
			});
		
		$(target).val("").trigger("change");
	},
	
	deleteRelationship: function() {
		$("tbody#relationship tr").eq($(".delete_relationship").index(this)).remove();
	},
	
	clear: function() {
		RelationshipExpense.source = [];
		RelationshipExpense.target = [];
		
		$(".add_relationship, .save_relationship").prop("disabled", true);
		$("tbody#relationship tr").remove();
	},
	
	loadExpense: function() {
		$("tbody#relationship tr:not(:first)").remove();
		$(".expense_target").empty();
		
		$.get(Routing.generate('relationship_expense_expense', {id: $("#expense_reference").val()}))
			.done(function(response) {
				RelationshipExpense.source = response.source;
				RelationshipExpense.target = response.target;

				RelationshipExpense.loadRelationship($("#expense_reference").val());
			})
			.fail(function() {
				Message.msgError("Error: fail to load content");
			});
	},
	
	saveRelationship: function() {
		var data = {
			expense_reference: $("#expense_reference").val(),
			expense_source: [],
			expense_target: {}
		};

		$("tbody#relationship tr").each(function(i, tr) {
			let value = $(tr).find(".expense_source").val();
			
			if (value) {
				data.expense_source.push(value);
				data.expense_target[value] = $(tr).find(".expense_target").val();
			}
		});
		
		$.post(
			Routing.generate('relationship_expense_save'),
			data
		)
		.done(function(response) {
			if (response) {
				Message.msgSuccess("Relationship saved successfully");
				RelationshipExpense.loadRelationship($("#expense_reference").val());
			} else {
				Message.msgError("Failure to save relationship");
			}
		})
		.fail(function() {
			Message.msgError("Error: fail to save content");
		});
	}
};

jQuery(document).ready(function() {
	RelationshipExpense.init();
});