Bank = {
    init: function() {
        this.configForm();
        this.configGrid();
    },

    configForm: function() {
        var form = $('.form form');
        var submit = function() {
            form.preventDefault();
            var obj = {
            };

            Form.submitAjax(form, obj);
            return false;
        };
        Form.addValidate(form, submit);
    },

    configGrid: function() {
    	$(".change-status").on("click", this.changeStatus);
    	$(".toggle-check").on("click", this.toggleCheck);
    },

    changeStatus: function() {
    	if (!$(".status-check:checked").length) {
    		Message.msgError('Select the row to change the status');
			return false;
    	}

    	var data = {
    		status: $(this).data("status"),
    		identifiers: []
    	};

    	$(".status-check:checked").each(function() {
    		data.identifiers.push($(this).val());
    	});

    	$.post(location.href.replace(/\/$/, "/status"), data)
    		.done(function() {
    			Message.msgSuccess('Status changed.');

    			var callback = function() {
    				document.location.reload(true);
    			};

    			window.setTimeout(callback, 1000);
    		}).fail(function() {
    			Message.msgError('The status could not be changed.');
    		});
    },

    toggleCheck: function()
    {
    	$(".status-check").each(function() {
    		$(this).prop("checked", !$(this).is(":checked"));
    	});
    }
};

// Excecute
$(document).ready(function() {
    Bank.init();
});
