SocialInstitution = {
    init: function() {
        // Index
        if ($('#tableSocialInstitution').length > 0) {
            this.listInit();
        }

        riot.mount('*');
    },

    listInit: function(){
        $('.select2').select2();        
        this.configGrid();
    },

    configGrid: function() {
        General.loadTable('#tableSocialInstitution', Routing.generate('social_institution_list'));

        $(".change-status").on("click", this.changeStatus);
        $(".toggle-check").on("click", this.toggleCheck);
    },

    changeStatus: function() {
        if (!$(".status-check:checked").length) {
            Message.msgError('Select the row to change the status');
            return false;
        }

        var data = {
            status: $(this).data("status"),
            identifiers: []
        };

        $(".status-check:checked").each(function() {
            data.identifiers.push($(this).val());
        });

        $.post(location.href.replace(/\/$/, "/status"), data)
            .done(function() {
                Message.msgSuccess('Status changed.');

                var callback = function() {
                    document.location.reload(true);
                };

                window.setTimeout(callback, 1000);
            }).fail(function() {
                Message.msgError('The status could not be changed.');
            });
    },

    toggleCheck: function() {
        $(".status-check").each(function() {
            $(this).prop("checked", !$(this).is(":checked"));
        });
    },

    checkBtnSave: function() {
        if ($('.tdSocialResponse').length <= 0) {
            $('#btnSave')
                .attr('disabled', true)
                .attr('title', Translator.trans('iss.tooltip.btn.disabled', {}, 'app'));
        } else {
            $('#btnSave')
                .attr('disabled', false)
                .attr('title', Translator.trans('iss.tooltip.btn.enabled', {}, 'app'));
        }
    },

    search: function(){
        General.loadTable(
            '#tableSocialInstitution',
            Routing.generate('social_institution_list'),
            null,
            $('#formSearch').serialize()
        );

        General.scrollTo('#formSearch');
    }
};


// Excecute
$(document).ready(function() {
    SocialInstitution.init();
});
