Expense = {
    init: function() {
        this.configSearch();
        this.configForm();
        this.configGrid();
        this.configTree();
    },

    configSearch: function() {
        var self = this;
        
        $("select.search").on('change', function(e) {
            e.preventDefault();
            self.configTree();
        });        
    },
    
    configForm: function() {
        var form = $('.form form');
        var submit = function() {
            form.preventDefault();
            var obj = {
            };

            Form.submitAjax(form, obj);
            return false;
        };
        Form.addValidate(form, submit);
    },
    
    configGrid: function() {
    	$(".change-status").on("click", this.changeStatus);
    	$(".toggle-check").on("click", this.toggleCheck);
    },
    
    getChildren: function(item) {
        var element = {
            "id": item.id,
            "text": item.code + ' - ' + item.name,
            "li_attr": {
                "class": parseInt(item.status) === 0 ? "inactive-item" : "", 
            },
            "children": []
        }
        
        if (item.status == 0) {
            element.icon = "fa fa-folder icon-state-danger";
        } else {
            element.icon = "fa fa-folder icon-state-warning";
        }
        
        if (item.children.length == 0) {
            return element;
        }
        
        element.state = {
            opened: true
        }
        
        for(var i=0; i<item.children.length; i++) {
            var expense = item.children[i];
            element.children.push(this.getChildren(expense));
        }
        
        return element;
    },
    
    configTree: function() {
        var self = this;
        
        $("#expense_tree").jstree("destroy");

    	$.get(
            Routing.generate('expense_list'),
            $("#search-form").serialize(),
            function(response) {
                
                var data = [];
                
                $.each(response, function(i, item) {
                    data.push(self.getChildren(item));
                });
                
                // -- inicio, cria tree view
                $("#expense_tree").jstree({
                    core : {
                        themes : {
                            responsive: true
                        }, 
                        // so that drag and drop works
                        check_callback : true,
                        data: data
                    },
                    types : {
                        default : {
                            "icon" : "fa fa-folder icon-state-warning icon-lg"
                        },
                        file : {
                            "icon" : "fa fa-file icon-state-warning icon-lg"
                        }
                    },
                    contextmenu: {
                        items: function() {
                            return {
                                edit: {
                                    label: "edit",
                                    action: function (data) {
                                        var ref = $.jstree.reference(data.reference);
                                        var obj = ref.get_node(data.reference);
                                        
                                        if (obj) {
                                            location.href = Routing.generate('expense_edit',{ id: obj.id })
                                        }
                                        
                                    }
                                }
                            }
                        }
                    },
                    checkbox: {
                        three_state: false
                    },
                    plugins: ["contextmenu", "editicon", "checkbox", "dnd", "types"],
                })
                .bind("move_node.jstree", function (e, data) {
                    // get reference for jsTree
                    var ref = $.jstree.reference(data.node);
                    
                    // get node dragged and parent
                    var node = ref.get_node(data.node);
                    var parent = ref.get_node(data.parent);
                    
                    // nodeId and parent
                    var nodeId = node.id;
                    var parentId = -1;
                    
                    if (parent.type !== '#' && !isNaN(parseInt(parent.id))) {
                        parentId = parseInt(parent.id);
                    }
                    
                    //
                    //-- make request to change the parent id of nodeId
                    //
                    var data = {
                        expenseId: nodeId,
                        newParentId: -1 === parentId ? null : parentId
                    }
                    
                    $.post(Routing.generate('expense_change_parent'), data)
                    .done(function() {
                        self.configTree();
                    }).fail(function() {
                            Message.msgError('Error while ordering expenses!');
                    });
                })
                .bind('edit_node.jstree', function (e, data) {
                    // get reference for jsTree
                    var ref = $.jstree.reference(data.node);
                    
                    // get node dragged and parent
                    var node = ref.get_node(data.node);
            
                    location.href = Routing.generate('expense_edit',{ id: node.id });
                });
                // -- fim, cria tree view
                
                
                
            }
        );
        
        

        
    },
    
    changeStatus: function() {
        var self = this;
        
        var selected = $("#expense_tree").jstree().get_selected(true);
        
    	if (!selected.length) {
            Message.msgError('Select the row to change the status');
            return false;
    	}
        
    	var data = {
    		status: $(this).data("status"),
    		identifiers: []
    	};
    	
    	$.each(selected, function(i, element) {
            data.identifiers.push(element.id);
    	});
    	
    	$.post(location.href.replace(/\/$/, "/status"), data)
        .done(function() {
                Message.msgSuccess('Status changed.');
                Expense.configTree();
        }).fail(function() {
                Message.msgError('The status could not be changed.');
        });

    },
    
    toggleCheck: function() {
    	$(".status-check").each(function() {
    		$(this).prop("checked", !$(this).is(":checked"));
    	});
    }
    
};

// Excecute
$(document).ready(function() {
    Expense.init();
});
