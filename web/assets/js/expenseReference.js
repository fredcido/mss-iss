ExpenseReference = {
    init: function() {
        this.configForm();
    },

    configForm: function() {
        var form = $('.form form');
        var submit = function() {
            form.preventDefault();
            var obj = {
            };

            Form.submitAjax(form, obj);
            return false;
        };
        Form.addValidate(form, submit);
    },
};

// Excecute
$(document).ready(function() {
    ExpenseReference.init();
});
