User = {
    init: function() {
        this.configForm();
    },

    configForm: function() {
        var form = $('#userForm form');
        var submit = function() {
            form.preventDefault();
            var obj = {
                //callback: Client.Client.afterSaveInformation
            };

            Form.submitAjax(form, obj);
            return false;
        };
        Form.addValidate(form, submit);
    },

    configList: function(){
        
    }
};

// Excecute
$(document).ready(function() {
    User.init();
});
