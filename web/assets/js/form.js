Form = {
    init: function() {
        $.fn.clearForm = function(disabled) {
            container = null;
            clearDisabled = disabled == undefined ? true : disabled;

            if ($(this).hasClass('btn'))
                container = $(this).closest('form');
            else
                container = $(this);

            Message.clearMessages(container);

            container.find(':input').each(
                function(index, e) {
                    if (!clearDisabled && $(this).attr('disabled'))
                        return true;

                    if ($(e).hasClass('no-clear'))
                        return true;

                    switch (e.type) {
                        case 'hidden':
                        case 'password':
                        case 'select-multiple':
                        case 'text':
                        case 'textarea':
                        case 'select-one':

                            $(e).val('');

                            if ($(e).hasClass('chzn-done'))
                                $(e).trigger('liszt:updated');
                        case 'checkbox':
                        case 'radio':
                            e.checked = false;
                            $(e).trigger('change');
                    }
                }
            );

            $.uniform.update(container.find(':input'));

            if (container.validate())
                container.validate().resetForm();

            container.find('.control-group.error').removeClass('error');
            container.find('.chosen').val('').trigger('change');

            container.trigger('clear');

            return this;
        };

        this.initCustomSelect();
        this.initReadOnlySelect();
        this.initMasks();
        this.initReset();
    },

    initCustomSelect: function() {
        $('select.chosen').select2({
            allowClear: true,
            placeholder: 'Choose one'
        });
    },

    initMasks: function() {
        this.extendMasks();

        // Date mask
        $('.date-mask').inputmask('d/m/y', {
            "clearIncomplete": true
        });

        // $(".date-mask").datepicker({
        //     format: "dd/mm/yyyy"
        // }).on('changeDate',
        //     function(ev) {
        //         $(this).focus();
        //         $(this).datepicker('hide');
        //         $(this).trigger('change');
        //     }
        // );

        // Montth YEar
        $('.month-year-mask').inputmask('m/y', {
            "clearIncomplete": true
        });

        // Year mask
        $('.year-mask').inputmask('y', {
            "clearIncomplete": true
        });

        // Time mask
        $('.time-mask').inputmask('h:s', {
            "clearIncomplete": true
        });

        // Tel mask
        $('.mobile-phone').inputmask('(670)9999-9999', {
            "clearIncomplete": true
        });
        $('.house-phone').inputmask('(670)9999-9999', {
            "clearIncomplete": true
        });
        $('.phone-mask').inputmask('(999)9999-9999', {
            "clearIncomplete": true
        });

        // only numbers mask
        $('.text-numeric').inputmask({
            'mask': '9',
            'repeat': 5,
            'greedy': false
        });

        // only number with 4 digits
        $('.text-numeric4').inputmask({
            'mask': '9',
            'repeat': 4,
            'greedy': false
        });

        $("[data-inputmask]").inputmask();
    },

    extendMasks: function() {
        // $.extend($.inputmask.defaults.aliases, {
        //     'hexa': {
        // 	alias: "numeric",
        // 	placeholder: '',
        // 	allowPlus: false,
        // 	allowMinus: false
        //     }
        // });
    },

    initReset: function() {
        $(document).on('click', '#clear.btn',
            function() {
                $(this).closest('form').clearForm();
            }
        );
    },

    initReadOnlySelect: function() {
        $('select').off('change.selectreadonly');

        $('select[readonly]').each(
            function() {
                $(this).data('prev', $(this).val());
                $(this).on(
                    'change.selectreadonly',
                    function(data) {
                        data.stopPropagation();
                        data.preventDefault();

                        var obj = $(this);

                        obj.val(obj.data('prev'));
                        obj.data('prev', obj.val());

                        return false;
                    }
                );
            }
        );
    },

    submitAjax: function(form, obj) {
        if (General.empty(obj.data))
            pars = $(form).serialize();
        else
            pars = obj.data.concat($(form).serializeArray());

        var action = $(form).attr('action');
        if (obj.action)
            action = obj.action;

        $.ajax({
            type: 'POST',
            data: pars,
            dataType: 'json',
            url: action,
            beforeSend: function() {
                Message.setFadeOut(false).msgInfo(Translator.trans('loading'), form);
                General.scrollTo(form);
            },
            success: function(response) {
                //Message.clearMessages(form);

                if (obj && obj.callback) {
                    response.form = form;
                    General.execFunction(obj.callback, response);
                }

                Form.showErrorsForm(form, response.errors);

                if (response.fields && response.fields.length) {

                    for (id in response.fields) {

                        var ele = $('#' + response.fields[id]);
                        ele.closest('.forms-group').addClass('has-error');
                    }
                }

                if (!response.status) {
                    if (General.empty(response.description))
                        Message.msgError('Something went wrong', form);

                } else if (General.empty(obj.nomsg)) {
                    Message.msgSuccess('Operation was a success', form);
                }

                if (!General.empty(response.description)) {
                    Message.msgError(response.description, form);
                }
            },
            error: function() {
                Message.clearMessages(form).msgError('Something went really wrong', form);

                if (obj && obj.callbackError)
                    General.execFunction(obj.callbackError, arguments);
            }
        });

        return false;
    },

    showErrorsForm: function(form, errors) {
        $(form).find('.forms-group.error').removeClass('has-error');
        $(form).find('.forms-group .help-inline').remove();

        for (id in errors) {

            var ele = form.find('#' + id);
            ele.closest('.forms-group').addClass('has-error');
        }
    },

    addErrorsMessages: function(form, errors) {
        $.each(errors, function(k, v) {
            errorsText = "";
            if ((v.errors) && (v.errors.length > 0)) {
                $.each(v.errors, function(n, errorText) {
                    errorsText += errorText;
                });
                $('#' + $(form).attr('name') + '_' + k).closest('div[class~="form-group"]').addClass('has-error');
                $('#' + $(form).attr('name') + '_' + k).closest('div[class~="form-group"]').append("<span class='help-inline'>" + errorsText + "</span>");
            }
        });
    },

    cloneForm: function(form) {
        var newForm = $(form).clone();
        newForm.addClass('form-report-clone').hide();
        $('body').append(newForm);

        $(form).find('select').each(
            function(index, node) {
                newForm.find('select').eq(index).val($(node).val());
            }
        );

        newForm.attr('method', 'post');

        return newForm;
    },

    getRequiredForm: function(form) {
        rules = {};
        $(form).find('label.required').each(
            function(index, element) {
                rules[$(element).attr('for')] = 'required';
            }
        );

        $(form).find('.chosen').each(
            function() {
                var label = $(this).closest('.form-group').find('label.required');
                if (label.length)
                    rules[$(this).attr('id')] = 'required';
            }
        );

        $(form).find('.form-group .required').each(
            function(index, element) {
                rules[$(element).attr('id')] = 'required';
            }
        );

        return rules;
    },

    addValidate: function(form, submit) {
        var validator = null;
        rules = Form.getRequiredForm(form);

        $.validator.messages.required = Translator.trans("not_blank", {}, "validators");

        validator = $(form).validate({
            errorElement: 'span',
            errorClass: 'text-danger',
            focusInvalid: false,
            ignore: "",
            rules: rules,

            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },

            invalidHandler: function(event, validator) {
                Message.clearMessages(form);
                Message.msgError(Translator.trans("check_required_fields"), form);
            },

            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },

            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            }
        });

        if(submit !== undefined){
            $(form).validate({submitHandler: submit});
        }

        return validator;
    },

    makeRequired: function(selector, required) {
        if (required) {

            $(selector).each(
                function() {
                    $(this).rules('add', 'required');
                    $(this).closest('.control-group').find('label').addClass('required');
                }
            );

        } else {

            $(selector).each(
                function() {
                    $(this).rules('remove', 'required');
                    $(this).closest('.control-group').find('label').removeClass('required');
                }
            );
        }

        $(selector).each(
            function() {
                $(this).closest('.control-group.error').removeClass('error').find('.help-block').remove();
            }
        );
    },

    showLoading: function() {
        waitingDialog.show();
    },

    hideLoading: function() {
        waitingDialog.hide();
    }
}

$(document).ready(
    function() {
        Form.init();
    }
);
