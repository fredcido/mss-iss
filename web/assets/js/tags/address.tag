<component-address>
    <div id="{ opts.id }">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label text-capitalize required">
                        { Translator.trans('municipios', {}, 'app') }
                    </label>
                    <select class="form-control municipio select2" name="{ opts.form_name }[municipio]" required>
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label text-capitalize  required">
                        { Translator.trans('postoAdministrativo', {}, 'app') }
                    </label>
                    <select class="form-control postoAdministrativo select2" name="{ opts.form_name }[posto_administrativo]" required></select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label text-capitalize  required">
                        { Translator.trans('sukus', {}, 'app') }
                    </label>
                    <select class="form-control suku select2" name="{ opts.form_name }[suku]" required></select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label text-capitalize  required">
                        { Translator.trans('address', {}, 'app') }
                    </label>
                    <input type="text" class="form-control address" name="{ opts.form_name }[address]" value="{ opts.model.address }" maxlength="255" required>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="control-label text-capitalize">
                        { Translator.trans('postalCode', {}, 'app') }
                    </label>
                    <input type="text" class="form-control postalCode" name="{ opts.form_name }[postal_code]" value="{ opts.model.postal_code }" maxlength="45">
                </div>
            </div>
        </div>
    </div>

    <script>
        var self = this;
        self.municipios = [];
        self.postosAdministrativos = [];
        self.sukus = [];
        self.model = {};

        mapDataToSelect2(data) {
            var newData = $.map(data, function (item) {
                return {id: item.id, text: item.name};
            });

            newData.unshift({id: '', text: ''});
            return newData;
        }

        getMunicipios() {
            $.ajax({method: 'GET', dataType: 'json', url: Routing.generate('data_municipios')}).done(function (response) {
                self.municipios = response;
                self.update();

                var data = self.mapDataToSelect2(response);
                var $el = $('#' + opts.id).find('.municipio');
                $el.select2({data: data}).on('change', function (e) {
                    self.getPostosAdministrativos($(this).val());
                });

                if(opts.model){
                    $el.val(opts.model.suku.posto_administrativo.municipio.id).trigger('change');
                    $('#' + opts.id).find('.postoAdministrativo').val(opts.model.suku.posto_administrativo.id).trigger('change');
                    $('#' + opts.id).find('.suku').val(opts.model.suku.id).trigger('change');
                }
            });
        }

        getPostosAdministrativos(municipio, value) {
            var $el = $('#' + opts.id).find('.postoAdministrativo');
            var $elSuku = $('#' + opts.id).find('.suku');

            $el.html('').select2({data: null});
            $elSuku.html('').select2({data: null});

            self.municipios.some(function (p) {
                if (municipio == p.id) {
                    self.postosAdministrativos = p.postos_administrativos;

                    var data = self.mapDataToSelect2(p.postos_administrativos);
                    $el.select2({data: data}).on('change', function (e) {
                        self.getSukus($(this).val());
                    });

                    if(value){
                        $el.val(value).trigger('change');
                    }
                }
            });

            self.update();
        }

        getSukus(posto_administrativo, value) {
            var $el = $('#' + opts.id).find('.suku');
            $el.html('').select2({data: null});

            self.postosAdministrativos.some(function (p) {
                if (posto_administrativo == p.id) {
                    self.sukus = p.sukus;

                    var data = self.mapDataToSelect2(p.sukus);
                    $el.select2({data: data});

                    if(value){
                        $el.val(value).trigger('change');
                    }
                }
            });

            self.update();
        }

        this.on('mount', function () {
            self.getMunicipios();
        });
    </script>
</component-address>
