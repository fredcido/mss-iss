<social-institution-tabs>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#tabInfo" aria-controls="tabInfo" role="tab" data-toggle="tab" data-selected="info">
                { Translator.trans('Info', {}, 'app') }
            </a>
        </li>
        <li role="presentation">
            <a href="#tabContacts" aria-controls="tabContacts" role="tab" data-toggle="tab" data-selected="contacts">
                { Translator.trans('Contacts', {}, 'app') }
            </a>
        </li>
        <li role="presentation">
            <a href="#tabBanks" aria-controls="tabBanks" role="tab" data-toggle="tab" data-selected="banks">
                { Translator.trans('Banks', {}, 'app') }
            </a>
        </li>
        <li role="presentation">
            <a href="#tabArea" aria-controls="tabArea" role="tab" data-toggle="tab" data-selected="areas">
                { Translator.trans('InterventionArea', {}, 'app') }
            </a>
        </li>
    </ul>
    <div class="tab-content" style="min-height:300px;">
        <div role="tabpanel" class="tab-pane active" id="tabInfo"></div>
        <div role="tabpanel" class="tab-pane" id="tabContacts"></div>
        <div role="tabpanel" class="tab-pane" id="tabBanks"></div>
        <div role="tabpanel" class="tab-pane" id="tabArea"></div>
    </div>
    <button type="button" class="btn btn-primary text-capitalize" disabled onclick="{ save }" id="btnSave" title="{ Translator.trans('iss.btn.disabled', {}, 'app') }">
        <i class="fa fa-save"></i>
        <span if="{ opts.id == '' }">{ Translator.trans('crud.action.create', {}, 'app') }</span>
        <span if="{ opts.id != '' }">{ Translator.trans('crud.action.edit', {}, 'app') }</span>
    </button>
    <a href="javascript:;" show="{ showPrevious }" class="btn btn-default text-capitalize" id="btnPrev">
        <i class="fa fa-arrow-left"></i>
        { Translator.trans('previous', {}, 'app') }
    </a>
    <a href="javascript:;" show="{ showNext }" class="btn btn-default text-capitalize" id="btnNext">
        <i class="fa fa-arrow-right"></i>
        { Translator.trans('next', {}, 'app') }
    </a>
    <script>
        var self = this;
        this.showPrevious = false;
        this.showNext = true;
        this.model = {};

        this.on('mount', function () {
            $('[data-toggle="tooltip"]').tooltip();
            Form.addValidate($('#formSocialInstitution'));
            self.wizard();

            if (opts.id != null && opts.id != '') {
                self.populate(opts.id);
            } else {
                riot.mount('#tabInfo', 'social-institution-info');
                riot.mount('#tabContacts', 'social-institution-contacts');
                riot.mount('#tabBanks', 'social-institution-banks');
                riot.mount('#tabArea', 'social-institution-intervention-areas');
            }

            self.localStorage();
            self.checkLocalStorage();
        });

        wizard() {

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var tabSelected = $(e.target).data('selected');
                self.checkBtnPrevious(tabSelected);
            });

            $('#btnNext').click(function () {
                if ($('#formSocialInstitution').valid()) {
                    var $a = $('.nav-tabs li.active').next('li').find('a');
                    self.checkBtnPrevious($a.data('selected'));
                    $a.trigger('click');
                }
            });

            $('#btnPrev').click(function () {
                var $a = $('.nav-tabs li.active').prev('li').find('a');
                self.checkBtnPrevious($a.data('selected'));
                $a.trigger('click');
            });
        }

        localStorage() {
            $(document).on(
                'change',
                ':input',
                function()
                {
                    var storageObject = storagejs.get( 'social_institution' );
                    storageObject.data = $('form').serializeJSON();
                    storageObject.save();
                }
            );
        }

        checkLocalStorage() {
            if (opts.id != null && opts.id != '') {
                return;
            }

            var storageObject = storagejs.get( 'social_institution' );
            if (!storageObject.data) {
                return;
            }

            var message = Translator.trans('ReloadPreviousContent', {}, 'app');
            if (!confirm(message)) {
                return;
            }

            var data = storageObject.data;
            self.model = data;
            self.mountPopulateChildrenTags(data);
        }

        checkBtnPrevious(tabSelected) {
            if (tabSelected == 'info') {
                self.showPrevious = false;
            } else {
                self.showPrevious = true;
            }

            if (tabSelected == 'areas') {
                self.showNext = false;
            } else {
                self.showNext = true;
            }

            self.update();
        }

        populate(id) {
            var $form = $('#formSocialInstitution');
            $.ajax({
                method: 'GET',
                dataType: 'json',
                url: Routing.generate('social_institution_retrieve', {'id': id}),
                data: $form.serialize(),
                beforeSend: function () {
                    General.blockContainer($form, Translator.trans('loading', {}, 'app'));
                }
            }).done(function (response) {
                var storageObject = storagejs.get( 'social_institution' );
                storageObject.data = null;
                storageObject.save();

                self.model = response;
                self.mountPopulateChildrenTags(response);
                General.unblockContainer($form);

                console.info(response);
            });
        }

        mountPopulateChildrenTags(model) {
            riot.mount('#tabInfo', 'social-institution-info', {'model': model});
            riot.mount('#tabContacts', 'social-institution-contacts', {'model': model.contacts});
            riot.mount('#tabBanks', 'social-institution-banks', {'model': model.bank_information});
            riot.mount('#tabArea', 'social-institution-intervention-areas', {'model': model.intervention_areas});
        }

        getErrorHTML(response) {
            var $container = $('<div>').append('<h4>' + response.text + '</h4>');
            var $ul = $('<ul class="nav">');
            self.getErrorForm(response.errors, $ul);
            return $container.append($ul).html();
        }

        getErrorForm(errors, $ul) {
            try {
                if (errors) {
                    for (var i in errors) {
                        var $li = $('<li>').appendTo($ul);
                        $li.append(Translator.trans(i, {}, 'app') + ': ');

                        if (typeof errors[i] === 'object') {
                            var $ulChild = $('<ul>').appendTo($li);
                            $li.append(self.getErrorForm(errors[i], $ulChild));
                        } else {
                            $li.append(Translator.trans(errors, {}, 'app'));
                        }
                    }
                }

                return $ul;
            } catch (e) {}
        }

        getCSSTypeResponse(type) {
            var css = type;
            if (type == 'error' || type == 'validation_error') {
                css = 'danger';
            }

            return css;
        }

        save(event) {
            event.preventDefault();
            var $form = $('form');

            if ($form.valid()) {
                $.ajax({
                    method: 'POST',
                    dataType: 'json',
                    url: Routing.generate('social_institution_save', {id: opts.id}),
                    data: $form.serialize()
                }).done(function (response) {
                    Message.showMsg(self.getErrorHTML(response), self.getCSSTypeResponse(response.type));
                    console.warn(response);
                }).fail(function(e){
                    Message.showMsg(Translator.trans('fail_ajax', {}, 'app'), 'danger');
                });
            }
        }
    </script>
</social-institution-tabs>
