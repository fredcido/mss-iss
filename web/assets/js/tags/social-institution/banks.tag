<social-institution-banks>
    <div class="row">
        <div class="col-sm-12">
            <button type="button" class="btn btn-info text-capitalize" name="button" onclick="{ addBank }">
                <i class="fa fa-plus"></i>
                { Translator.trans('add', {}, 'app') }
            </button>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-sm-12">
            <div each="{ bankInformation, i in banksInformations }">
                <div class="portlet light bordered divBankInformation">
                    <div class="portlet-title">
                        <div class="caption font-blue-sharp">
                            <span class="caption-subject bold text-uppercase">
                                { Translator.trans('Bank', {}, 'app') }
                            </span>
                        </div>
                        <div class="actions">
                            <a href="javascript:;" onclick="{ removeBank }" class="btn btn-circle btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                                { Translator.trans('remove', {}, 'app') }
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label text-capitalize required">
                                        { Translator.trans('Bank', {}, 'app') }
                                    </label>
                                    <select class="form-control" name="bank_information[{ i }][bank]">
                                        <option each="{ banks }" value="{ id }" selected="{ bankInformation.bank.id == id }">{ name }</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label text-capitalize required">
                                        { Translator.trans('name', {}, 'app') }
                                    </label>
                                    <input type="text" name="bank_information[{ i }][name]" maxlength="21" class="form-control" value="{ bankInformation.name }" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label text-capitalize required">
                                        { Translator.trans('nib', {}, 'app') }
                                    </label>
                                    <input type="text" name="bank_information[{ i }][nib]" maxlength="21" class="form-control" value="{ bankInformation.nib }" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label text-capitalize required">
                                        { Translator.trans('iban', {}, 'app') }
                                    </label>
                                    <input type="text" name="bank_information[{ i }][iban]" value="{ bankInformation.iban }" maxlength="45" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var self = this;
        this.banks = [];
        this.banksInformations = opts.model || [];

        this.on('mount', function () {
            this.getBanks();
        });

        addBank(event) {
            self.banksInformations.push({});
            General.scrollTo($(this.root).find('.divBankInformation:last'));
        }

        removeBank(event) {
            self.banksInformations.some(function (p) {
                if (event.item.bankInformation === p) {
                    self.banksInformations.splice(self.banksInformations.indexOf(p), 1);
                }
            });
        }

        getBanks() {
            $.ajax({method: 'GET', dataType: 'json', url: Routing.generate('data_banks')}).done(function (response) {
                self.banks = response;
                self.update();
            });
        }
    </script>
</social-institution-banks>
