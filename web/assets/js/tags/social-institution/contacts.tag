<social-institution-contacts>
    <div class="row">
        <div class="col-sm-12">
            <button type="button" class="btn btn-info text-capitalize" name="button" onclick="{ addContact }">
                <i class="fa fa-plus"></i>
                { Translator.trans('add', {}, 'app') }
            </button>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-sm-12">
            <div each="{ contact, i in contacts }">
                <div class="portlet light bordered divContacts">
                    <div class="portlet-title">
                        <div class="caption font-blue-sharp">
                            <span class="caption-subject bold uppercase">
                                { Translator.trans('Contact', {}, 'app') }
                            </span>
                        </div>
                        <div class="actions">
                            <a href="javascript:;" onclick="{ removeContact }" class="btn btn-circle btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                                { Translator.trans('remove', {}, 'app') }
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label text-capitalize required">
                                        { Translator.trans('name', {}, 'app') }
                                    </label>
                                    <input type="text" name="contacts[{ i }][name]" maxlength="255" class="form-control" value="{ contact.name }" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label  text-capitalize required">
                                        { Translator.trans('telephone', {}, 'app') }
                                    </label>
                                    <input type="text" data-inputmask="'mask': '(670) 9999-9999'" name="contacts[{ i }][telephone]" value="{ contact.telephone }" maxlength="45" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label text-capitalize ">
                                        { Translator.trans('post', {}, 'app') }
                                    </label>
                                    <select class="form-control" name="contacts[{ i }][post]">
                                        <option value=""></option>
                                        <option each="{ posts }" value="{ id }" selected="{ contact.post.id == id }">{ name }</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label text-capitalize">
                                        { Translator.trans('email', {}, 'app') }
                                    </label>
                                    <input type="email" name="contacts[{ i }][email]" value="{ contact.email }" maxlength="255" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var self = this;
        self.contacts = opts.model || [];
        self.posts = [];

        this.on('mount', function () {
            this.getPosts();
        });

        addContact(event) {
            self.contacts.push({});
            General.scrollTo($(this.root).find('.divContacts:last'));
        }

        removeContact(event) {
            self.contacts.some(function (p) {
                if (event.item.contact === p) {
                    self.contacts.splice(self.contacts.indexOf(p), 1);
                }
            });
        }

        getPosts() {
            $.ajax({
                method: 'GET',
                dataType: 'json',
                url: Routing.generate('data_posts')
            }).done(function (response) {
                self.posts = response;
                self.update();
            });
        }
    </script>
</social-institution-contacts>
