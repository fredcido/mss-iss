<social-institution-intervention-areas>
    <div class="input-group">
        <span class="input-group-btn">
            <button type="button" class="btn btn-info text-capitalize" name="button" onclick="{ addInterventionArea }">
                <i class="fa fa-plus"></i>
                { Translator.trans('add', {}, 'app') }
            </button>
        </span>
        <select style="width:100%;" class="form-control select2" id="selectInterventionArea">
            <option each="{ comboAreas }" value="{ id }">{ name }</option>
        </select>
    </div>
    <br/>
    <div class="row">
        <div class="col-sm-12">
            <div each="{ area, i in interventionAreas }" class="areas">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">{ area.intervention_area.name }</div>
                        <div class="tools">
                            <a href="javascript:;" id="btnTooglePortlet" class="collapse" title=""></a>
                            <a href="javascript:;" class="fullscreen" title=""></a>
                        </div>
                        <div class="actions">
                            <a href="javascript:;" onclick="{ addSocialResponse }" class="btn btn-circle btn-info btn-sm">
                                <i class="fa fa-plus"></i>
                                { Translator.trans('add', {}, 'app') }
                            </a>
                            <a href="javascript:;" onclick="{ removeInterventionArea }" class="btn btn-circle btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                                { Translator.trans('remove', {}, 'app') }
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body" style="padding:0px !important;">
                        <input type="hidden" name="intervention_areas[{ i }][intervention_area]" value="{ area.intervention_area.id }">
                        <div style="width:100%;overflow-x:auto;">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <th class="text-capitalize">
                                            { Translator.trans('SocialResponse', {}, 'app') }
                                        </th>
                                        <th class="text-capitalize">
                                            { Translator.trans('supportMss', {}, 'app') }
                                        </th>
                                        <th class="text-capitalize">
                                            { Translator.trans('placeOwnership', {}, 'app') }
                                        </th>
                                        <th class="text-capitalize">
                                            %
                                        </th>
                                        <th class="text-capitalize">
                                            { Translator.trans('districts', {}, 'app') }
                                        </th>
                                        <th class="text-capitalize">
                                            { Translator.trans('men', {}, 'app') }
                                        </th>
                                        <th class="text-capitalize">
                                            { Translator.trans('women', {}, 'app') }
                                        </th>
                                        <th class="text-capitalize">
                                            { Translator.trans('total', {}, 'app') }
                                        </th>
                                        <th style="max-width:70px;">
                                            { Translator.trans('actions', {}, 'app') }
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="tdSocialResponse" each="{ socialResponse, i in area.social_responses }">
                                        <td>
                                            { print(parent.i, i, '.id', 'text') }
                                        </td>
                                        <td>
                                            { print(parent.i, i, '.supportMss', 'text') }
                                        </td>
                                        <td>
                                            { print(parent.i, i, '.placeOwnership', 'text') }
                                        </td>
                                        <td>
                                            { print(parent.i, i, '.percentage') }
                                        </td>
                                        <td>
                                            { print(parent.i, i, '.districts', 'text', true) }
                                        </td>
                                        <td>
                                            { print(parent.i, i, '.men') }
                                        </td>
                                        <td>
                                            { print(parent.i, i, '.women') }
                                        </td>
                                        <td>
                                            { print(parent.i, i, '.total') }
                                        </td>
                                        <td>
                                            <button type="button" title="{ Translator.trans('crud.action.edit', {}, 'app') }" class="btn btn-primary btn-sm btn-circle" data-toggle="modal" data-target="#{ parent.i }_{ i }">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button type="button" title="{ Translator.trans('crud.action.delete', {}, 'app') }" class="btn btn-danger btn-sm btn-circle" onclick="{ removeSocialResponse }">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <social-institution-social-response-form
                                                parent-ui="{ parent.i }"
                                                ui="{ i }"
                                                support-mss="{ parent.parent.supportMss }"
                                                places-ownerships="{ parent.parent.placesOwnerships }"
                                                districts="{ parent.parent.districts }"
                                                modal-name="{ parent.i }_{ i }"
                                                model="{ socialResponse }"
                                                intervention_area_id="{ parent.area.intervention_area.id }">
                                            </social-institution-social-response-form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var self = this;
        this.interventionAreas = opts.model || [];
        this.comboAreas = [];
        this.placesOwnerships = [];
        this.supportMss = [];
        this.districts = [];

        getDistricts() {
            $.ajax({method: 'GET', dataType: 'json', url: Routing.generate('data_municipios')}).done(function (response) {
                self.districts = response;
                self.update();
            });
        }

        getPlaceOwnership() {
            $.ajax({method: 'GET', dataType: 'json', url: Routing.generate('data_place_ownership')}).done(function (response) {
                self.placesOwnerships = response;
                self.update();
            });
        }

        getSupportMss() {
            $.ajax({method: 'GET', dataType: 'json', url: Routing.generate('data_support_mss')}).done(function (response) {
                self.supportMss = response;
                self.update();
            });
        }

        getAreas() {
            $.ajax({method: 'GET', dataType: 'json', url: Routing.generate('data_intervention_areas')}).done(function (response) {
                self.comboAreas = response;
                self.update();
            });
        }

        hasInterventionArea(id) {
            var hasItem = false;
            self.interventionAreas.some(function (p) {
                if (id == p.intervention_area.id) {
                    hasItem = true;
                }
            });

            return hasItem;
        }

        addInterventionArea(event) {
            self.comboAreas.some(function (p) {
                if (self.selectInterventionArea.value == p.id) {
                    if (self.hasInterventionArea(this.selectInterventionArea.value)) {
                        var message = Translator.trans('InterventionArea_Exists', {}, 'app');
                        Message.showMsg(message, 'warning');
                    } else {
                        self.interventionAreas.push({
                            intervention_area: p,
                            social_responses:[]
                        });
                    }
                }
            });

            General.scrollTo($(this.root).find('.areas:last'));
        }

        removeInterventionArea(event) {
            self.interventionAreas.some(function (p) {
                if (event.item.area === p) {
                    self.interventionAreas.splice(self.interventionAreas.indexOf(p), 1);
                }
            });
            self.update();
            SocialInstitution.checkBtnSave();
        }

        addSocialResponse(event) {
            event.item.area.social_responses.push({});
            self.update();
            SocialInstitution.checkBtnSave();
        }

        removeSocialResponse(event) {
            self.interventionAreas.some(function (p) {
                p.social_responses.some(function(i){
                    if (event.item.socialResponse === i) {
                        p.social_responses.splice(p.social_responses.indexOf(i), 1);
                    }
                })
            });
            self.update();
            SocialInstitution.checkBtnSave();
        }

        print(parentUi, ui, name, type, multiple){
            var retorno = '-';
            var $el = $('#' + parentUi + '_' + ui);

            if(type == undefined || type == 'val'){
                retorno = $el.find(name).val();
            }
            else{
                if(multiple == true){
                    var options = []
                    $el.find(name + ' option:selected').each(function(i, e){
                        options.push($(e).text());
                    });
                    retorno = options.join(', ');
                }
                else{
                    retorno = $el.find(name + ' option:selected').text();
                }
            }
            return retorno;
        }

        self.on('mount', function () {
            self.getAreas();
            self.getPlaceOwnership();
            self.getSupportMss();
            self.getDistricts();

            SocialInstitution.checkBtnSave();
            $('.select2').select2();
        });
    </script>
</social-institution-intervention-areas>
