<social-institution-social-response-form>
    <!-- Modal -->
    <div class="modal fade bs-modal-lg" id="{ opts.modalName }" role="dialog" tabindex="-1" data-backdrop="static" aria-hidden="true" data-keyboard="false">
        <div class="modal-dialog modal-lg" role="document">
            <form onsubmit="{ saveDialog }" novalidate>
                <div class="modal-content">
                    <div class="portlet light bordered divSocialResponse">
                        <div class="portlet-title">
                            <div class="caption font-blue-sharp">
                                <span class="caption-subject bold uppercase">
                                    { Translator.trans('SocialResponse', {}, 'app') }
                                </span>
                            </div>
                            <div class="actions">
                                <button type="submit" class="btn btn-circle btn-info">
                                    <i class="fa fa-save"></i>
                                    { Translator.trans('crud.action.save', {}, 'app') }
                                </button>
                                <button type="button" class="btn btn-circle btn-danger" data-dismiss="modal">
                                    { Translator.trans('close', {}, 'app') }
                                </button>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label text-capitalize  required">
                                            { Translator.trans('SocialResponse', {}, 'app') }
                                        </label>
                                        <select class="form-control id select2" name="intervention_areas[{ opts.parentUi }][social_responses][{ opts.ui }][social_response]">
                                            <option each="{ socialResponses }" value="{ id }" selected="{ model.social_response.id == id }">{ name }</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label text-capitalize  required">
                                            { Translator.trans('supportMss', {}, 'app') }
                                        </label>
                                        <select class="form-control supportMss" name="intervention_areas[{ opts.parentUi }][social_responses][{ opts.ui }][supportMss]" required>
                                            <option value=""></option>
                                            <option each="{ opts.supportMss }" value="{ id }" selected="{ model.support_mss == id }">{ name }</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label text-capitalize  required">
                                            %
                                        </label>
                                        <input
                                            class="form-control percentage inputNumber"
                                            data-inputmask="'alias': 'decimal', 'groupSeparator': ','"
                                            type="text"
                                            value="{ model.percentage }"
                                            name="intervention_areas[{ opts.parentUi }][social_responses][{ opts.ui }][percentage]"
                                            maxlength="3"
                                            required>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label text-capitalize required">
                                            { Translator.trans('placeOwnership', {}, 'app') }
                                        </label>
                                        <select class="form-control placeOwnership" name="intervention_areas[{ opts.parentUi }][social_responses][{ opts.ui }][placeOwnership]" required>
                                            <option value=""></option>
                                            <option each="{ opts.placesOwnerships }" value="{ id }" selected="{ model.place_ownership == id }">{ name }</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <component-address
                                        id="{ opts.modalName }_address"
                                        model="{ model.address }"
                                        form_name="intervention_areas[{ opts.parentUi }][social_responses][{ opts.ui }][address]">
                                    </component-address>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label text-capitalize  required">
                                            { Translator.trans('districts', {}, 'app') }
                                        </label>
                                        <select class="form-control select districts select2" name="intervention_areas[{ opts.parentUi }][social_responses][{ opts.ui }][districts][]" multiple="multiple">
                                            <option each="{ opts.districts }" value="{ id }" selected="{ selectedDistrict(id) }">{ name }</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label text-capitalize  required">
                                            { Translator.trans('men', {}, 'app') }
                                        </label>
                                        <input class="form-control men inputNumber" name="intervention_areas[{ opts.parentUi }][social_responses][{ opts.ui }][men]" value="{ model.men }" onkeyup="{ calcTotal }" type="text" maxlength="10" required>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label text-capitalize  required">
                                            { Translator.trans('women', {}, 'app') }
                                        </label>
                                        <input class="form-control women inputNumber" name="intervention_areas[{ opts.parentUi }][social_responses][{ opts.ui }][women]" value="{ model.women }" onkeyup="{ calcTotal }" type="text" maxlength="10" required>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label text-capitalize  required">
                                            { Translator.trans('total', {}, 'app') }
                                        </label>
                                        <input class="form-control total" name="intervention_areas[{ opts.parentUi }][social_responses][{ opts.ui }][total]" value="{ model.total }" readonly type="text" maxlength="10" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label text-capitalize  required">
                                            { Translator.trans('observation', {}, 'app') }
                                        </label>
                                        <textarea class="form-control observation" name="intervention_areas[{ opts.parentUi }][social_responses][{ opts.ui }][observation]" rows="2" required>{ model.observation }</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        var self = this;
        this.model = opts.model || {};
        this.socialResponses = [];

        saveDialog(event) {
            event.preventDefault();

            if ($(this.root).find('form').valid()) {
                $('#' + opts.modalName).modal('toggle');
                self.update();
            }
        }

        closeDialog(event) {
            if ($(this.root).find('form').valid()) {
                $('#' + opts.modalName).modal('toggle');
            }
        }

        calcTotal(event) {
            var men = parseInt($('#' + opts.modalName).find('.men').val()) || 0;
            var women = parseInt($('#' + opts.modalName).find('.women').val()) || 0;
            var $total = $('#' + opts.modalName).find('.total');
            $total.val(men + women);
        }

        getSocialResponses(intervention_area_id) {
            self.parent.interventionAreas.some(function (i) {
                if (i.intervention_area.id == intervention_area_id) {
                    self.socialResponses = i.intervention_area.social_response;
                }
            });
        }

        selectedDistrict(id){
            var selected = false;

            if(id){
                self.model.districts.some(function(i){
                    if(i.id == id){
                        selected = true;
                        return;
                    }
                });
            }

            return selected;
        }

        this.on('mount', function () {
            Form.addValidate($(this.root).find('form'));
            self.getSocialResponses(opts.intervention_area_id);

            setTimeout(function () {
                if (!opts.model.id) {
                    $('#' + opts.modalName).modal('toggle');
                }
                $('.inputNumber').inputmask({'alias': 'decimal', 'groupSeparator': ','});
                $('.select2').select2();
            }, 200);            
        });
    </script>
</social-institution-social-response-form>
