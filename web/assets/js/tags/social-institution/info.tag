<social-institution-info>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label text-capitalize required">
                    { Translator.trans('name', {}, 'app') }
                </label>
                <input type="text" maxlength="255" class="form-control" name="name" value="{ opts.model.name }" required>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label text-capitalize required">
                    { Translator.trans('status', {}, 'app') }
                </label>
                <select class="form-control" name="institution_status" required>
                    <option value=""></option>
                    <option each="{ institutionsStatus }" value="{ id }" selected="{ id == parent.opts.model.institution_status.id }">{ name }</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <component-address
                form_name="address"
                id="info_address"
                model="{ opts.model.address }">
            </component-address>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label text-capitalize ">
                    { Translator.trans('email', {}, 'app') }
                </label>
                <input type="hidden" name="id" value="{ opts.model.id }">
                <input type="email" maxlength="255" class="form-control" name="email" value="{ opts.model.email }">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label text-capitalize ">
                    { Translator.trans('website', {}, 'app') }
                </label>
                <input type="text" maxlength="255" class="form-control" name="website" value="{ opts.model.website }">
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group" each="{ description, i in descriptionTypes }">
                <label class="control-label text-capitalize ">
                    { description.name }
                </label>
                <input type="hidden" name="descriptions[{ i }][description_type]" value="{ description.id }">
                <textarea class="form-control" name="descriptions[{ i }][description]" rows="3" cols="40" maxlength="500">
                    { parent.opts.model.descriptions[i].description }
                </textarea>
            </div>
        </div>
    </div>
    <script>
        var self = this;
        this.institutionsStatus = [];

        getDescriptionTypes() {
            $.ajax({
                method: 'GET',
                dataType: 'json',
                url: Routing.generate('data_description_types')
            }).done(function (response) {
                self.descriptionTypes = response;
                self.update();
            });
        }

        getInstitutionStatus() {
            $.ajax({
                method: 'GET',
                dataType: 'json',
                url: Routing.generate('data_institution_status')
            }).done(function (response) {
                self.institutionsStatus = response;
                self.update();
            });
        }

        this.on('before-mount', function () {
            this.getDescriptionTypes();
            this.getInstitutionStatus();
        });
    </script>
</social-institution-info>
