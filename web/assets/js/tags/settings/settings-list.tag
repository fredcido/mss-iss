<settings-list>
    
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-hover">
                <thead>
                    <th></th>
                    <th>{ Translator.trans('name', {}, 'app')}</th>
                    <th>{ Translator.trans('value', {}, 'app')}</th>
                    <th>{ Translator.trans('explanation', {}, 'app')}</th>
                </thead>
                <tbody>
                    <tr class="tr" each={all_settings}>
                        <td class="td-actions">
                            <div class={buttons: !isEditing(id) }>
                                <button 
                                    class={btn: true, btn-xs: true, btn-primary: true, setHidden: isEditing(id) } 
                                    onclick="{ () => { this.edit(id) } }">
                                        <i class="fa fa-pencil"></i> { Translator.trans('crud.action.edit', {}, 'app')}
                                </button>
                                <button 
                                    class={btn: true, btn-xs: true, btn-primary: true, setHidden: !isEditing(id) }
                                    onclick="{ () => { this.save(id) } }" disabled={ this.isSaving }>
                                        <i class="fa fa-save"></i> { Translator.trans('crud.action.save', {}, 'app')}
                                </button>
                            </div>
                        </td>
                        <td> <span>{ name }<span> </td>
                        <td>
                            <span class={setHidden: isEditing(id) }>{ value }</span>
                            <form class={setHidden: !isEditing(id) } data-id="{ id }" onsubmit="{ submitForm }" novalidate>
                                <input type="hidden" name="_token" value="{ this.options.token }"/>
                                <input type="text" class="form-control edit-field " name="value" value="{ value }" disabled={ this.isSaving }/>
                            </form>
                        </td>
                        <td> { explanation } </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <script>
        var self = this;

        // hold the id of current edditings
        this.editing_itens = [];

        // true when request to save has been sent
        this.isSaving = false;

        this.options = [];

        // set the value for this.isSaving
        setIsSaving(value) {
            self.isSaving = value;
            self.update;
        }

        // returns a list of all settings
        getAllSettings() {
            $.ajax({
                method: 'GET',
                dataType: 'json',
                url: Routing.generate('data_settings')
            }).done(function (response) {
                self.all_settings = response
                self.update()
            });
        }

        // checks if some item is being edited
        isEditing(id) {
            for(var i=0; i<this.editing_itens.length; i++) {
                if (id == this.editing_itens[i]) return true;
            }
            return false;
        }

        // start editing some item
        edit(id) {
            this.editing_itens.push(id);
            self.update();
        }

        // remove the id from the editing list
        finishEditing(id) {

            var index = -1;
            for(var i=0; i<this.editing_itens.length; i++) {
                if (id == this.editing_itens[i]) {
                    index = i;
                    break;
                }
            }
            
            if (index == -1) {
                return;
            }

            this.editing_itens.splice(index, 1);
            self.update();
        }

        // click on save button
        save(id) {
            $("form[data-id='" + id + "']").submit();
        }

        getErrorForm(response) {
            var $container = $('<div>').append('<h4>' + response.text + '</h4>');
            var $ul = $('<ul>').appendTo($container);
            var errors = response.errors

            try {
                if (errors != null || errors != '') {
                    for (var i in errors.children) {
                        var $li = $('<li>').append(Translator.trans(i, {}, 'app') + ': ');
                        for (var j in errors.children[i].form) {
                            $li.append(errors.children[i].form[j]);
                        }
                        $li.appendTo($ul);
                    }
                }
            } catch (e) {}

            return $container.html();
        }

        getCSSTypeResponse(type) {
            var css = type;
            if (type == 'error' || type == 'validation_error') {
                css = 'danger';
            }

            return css;
        }

        // on form submit
        submitForm(event) {
            event.preventDefault;

            var $form = $(event.currentTarget);

            if (!$form.valid()) {
                Message.showMsg("Form is not valid!", 'danger');
                return;
            }

            self.setIsSaving(true);

            // submit form to api
            $.ajax({
                method: 'POST',
                dataType: 'json',
                url: Routing.generate('setting_save', {id: $form.attr('data-id')}),
                data: $form.serialize()
            }).done(function (response) {
                self.finishEditing($form.attr('data-id'));
                self.setIsSaving(false)

                Message.showMsg(self.getErrorForm(response), self.getCSSTypeResponse(response.type));

                // reload settings
                self.getAllSettings();

            }).always(function() {
                self.setIsSaving(false);
            });

        }

        this.on('mount', function () {
            self.getAllSettings()
            self.options = this.opts;
            self.update()
        });
        
    </script>

    <style scoped>
        .tr > td > span {
            line-height: 35px;
        }

        .tr > td > div {
            padding-top: 8px;
        }

        .tr > td:last-child {
            vertical-align: middle;
        }

        .td-actions {
            width: 80px;
        }

        .edit-field {
            width: 200px;
        }

        /* by default, all buttons are invisible */
        .buttons {
            display: none;
        }

        /* show div buttons on hover */
        table > tbody > tr:hover .td-actions > .buttons {
            display: block;
        }

        /* helper class to apply on buttons */
        .setHidden { display: none; }

    </style>

</settings-list>
