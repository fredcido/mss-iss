# Follow these instructions to set up your dev enviroment


### 1. Install php 
```
 sudo apt-get install php
```
*
NOTE: for php7, also install the xml extension*
```
sudo apt-get install php7.0-xml php7.0-mysql php7.0-intl
```
### 2. Download composer (https://getcomposer.org/)

### 3. Enter in the project folder and run composer install to install the packages
```
cd miss-iss
composer install
```

### 4. Install mysql
```
sudo apt-get install mysql-server
```

### 5. Copy the file parameter.yml.dist to parameter.yml and change the mysql parameters
```
cp app/config/parameters.yml.dist app/config/parameters.yml
```

### 6. Create the database and update the schema
```
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
```

### 7. Execute bower
```
 bower install
```

### 8. Run the server
```
php bin/console server:run
```



# JMS Translation Bundle - In order to translate messages, follow these steps:

### 1. Extract the messages to be translated in each language
```
php bin/console translation:extract en --config=app
php bin/console translation:extract pt --config=app
php bin/console translation:extract tl --config=app
```

### 2. Next, access the address http://127.0.0.1:8000/_trans, and translate the messages.
```
php bin/console server:run
```

### 3. navigate to http://127.0.0.1:8000/_trans

### 4. Stop the dev server, clear the symfony cache and start the server again
```
php bin/console cache:clear
php bin/console server:run
```



# Bazinga JS Translation - to make translations avaliable in JS

### 1. Execute these compands to compile and bundle the translations into js
```
php bin/console bazinga:js-translation:dump ./web/assets/js
php bin/console assetic:dump
```
